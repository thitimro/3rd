<?php
require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller {
  public function clientRequest_post() {
    //to get header 
    $headers=array();
    foreach (getallheaders() as $name => $value) {
        $headers[$name] = $value;
    }
    //to get post data
    $entityBody = file_get_contents('php://input', 'r');
    parse_str($entityBody , $post_data);

    //giving response back to client 
    $this->response('success', 200);


  }
}