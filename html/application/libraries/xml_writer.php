<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class used to write XMl document.
 * Extends XMLWriter (PHP5 only!)
 * 
 * Initialize the class:
 * $this->load->library('MY_Xml_writer');
 * $xml = new MY_Xml_writer;
 * $xml->initiate();
 * 
 * Start a branch with attributes:
 * $xml->startBranch('car', array('country' => 'usa', 'type' => 'racecar'));
 * 
 * End (close) a branch
 * $xml->endBranch();
 * 
 * Add a CDATA node with attributes:
 * $xml->addNode('model', 'Corolla', array('year' => '2002'), true);
 * 
 * Print the XMl directly to screen:
 * $xml->getXml(true);
 * 
 * Pass the XMl to a view file:
 * $data['xml'] = $xml->getXml();
 * $this->load->view('xml_template', $data);
 * 
 * @name /library/Accent/Xml/Accent_Xml_Writer.php
 * @category Accent_application
 * @version 1.0
 * @author Joost van Veen
 * @copyright Accent Webdesign
 * @created: 10 mrt 2009
 */
class Xml_writer extends XMLWriter
{
    
    /**
     * Name of the root element for this XMl. Defaults to root.
     */
    private $_rootName = '';
    
    /**
     * XML version. Defaults to 1.0
     */
    private $_xmlVersion = '1.0';
    
    /**
     * Character set. Defaukts to UTF-8.
     */
    private $_charSet = 'UTF-8';
    
    /**
     * Indent for every new tag. Defaults to spaces.  
     */
    private $_indentString = '    ';
    
    /**
     * Sets an xslt path for this XML. Defaults to ''. 
     * Xslt will only be included in the XML if $_xsltFilePath is not an 
     * empty string.
     */
    private $_xsltFilePath = '';

    public function __construct ()
    {}

    /**
     * Set the value of the root tag for this XML
     */
    public function setRootName ($rootName)
    {
        $this->_rootName = $rootName;
    }

    /**
     * Set the value of the XMl version for this XML.
     */
    public function setXmlVersion ($version)
    {
        $this->_xmlVersion = $version;
    }

    /**
     * Set the character set for this XML.
     */
    public function setCharSet ($charSet)
    {
        $this->_charSet = $charSet;
    }

    /**
     * Set indenting for every new node in this XML.
     */
    public function setIndentStr ($indentString)
    {
        $this->_indentString = $indentString;
    }

    /**
     * Set the XSLT filepath for this XML. This should be an absolute URL.
     */
    public function setXsltFilePath ($xsltFilePath)
    {
        $this->_xsltFilePath = $xsltFilePath;
    }

    public function initiate ()
    {
        // Create new xmlwriter using memory for string output.
        $this->openMemory();
        
        // Set indenting, if any.
        if ($this->_indentString) {
            $this->setIndent(true);
            $this->setIndentString($this->_indentString);
        }
        
        // Set DTD.
        $this->startDocument($this->_xmlVersion, $this->_charSet);
        
        // Set XSLT stylesheet path, if any.
        if ($this->_xsltFilePath) {
            $this->writePi('xml-stylesheet', 'type="text/xsl" href="' . $this->_xsltFilePath . '"');
        }
        
        // Set the root tag.
        $this->startElement($this->_rootName);
    }

    /**
     * Start a new branch that will contain nodes.
     */
    public function startBranch ($name, $attributes = array())
    {
        $this->startElement($name);
        $this->_addAttributes($attributes);
    }

    /**
     * End an open branch. A branch needs to be closed explicitely if the branch 
     * is followed directly by another branch.
     */
    public function endBranch ()
    {
        $this->endElement();
    }

    /**
     * Add a node, typically a child to a branch.
     * 
     * If you wish to create a simple text node, just set $name and $value.
     * If you wish to create a CDATA node, set $name, $value and $cdata.
     * You can set attributes for every node, passing a key=>value $attributes array
     * 
     * @param string $name
     * @param string $value
     * @param array attributes
     * @param boolean $cdata
     * @return void
     */
    public function addNode ($name, $value, $attributes = array(), $cdata = false)
    {
        /**
         * Set a CDATA element.
         */
        if ($cdata) {
            $this->startElement($name);
            $this->_addAttributes($attributes);
            $this->writeCdata($value);
            $this->endElement();
        }
        /**
         * Set a simple text element.
         */
        else {
            $this->startElement($name);
            $this->_addAttributes($attributes);
            $this->text($value);
            $this->endElement();
        }
    }

    /**
     * Close the XML document, print to screen if $echo == true, and return a 
     * string containing the full XML.
     * 
     * @param boolean echo - if true, print XML to screen. 
     * @return string - The full XML.
     */
    public function getXml ($echo = false)
    {
        
        /**
         * Set header.
         */
        if ($echo == true) {
            header('Content-type: text/xml');
        }
        
        /**
         * Close XMl document.
         */
        $this->endElement();
        $this->endDocument();
        
        /**
         * Return or echo output.
         */
        $output = $this->outputMemory();
        if ($echo == true) {
            // Print XML to screen.
            print $output;
        }
        
        return $output;
    }

    /**
     * Add attributes to an element.
     * 
     * @param array $attributes
     */
    private function _addAttributes ($attributes)
    {
        if (count($attributes) > 0) {
            // We have attributes, let's set them
            foreach ($attributes as $key => $value) {
                $this->writeAttribute($key, $value);
            }
        }
    }
    public function addDataProduct(){
       #$prdNo = '271761';
		$shopId = $_POST['shopId'];
        $advrtStmt = $_POST['advrtStmt'];
        $asDetail = $_POST['asDetail'];
        $bndlDlvCnYn = $_POST['bndlDlvCnYn'];
        $cupnDscMthdCd = $_POST['cupnDscMthdCd'];
        $cupnIssEndDy = $_POST['cupnIssEndDy'];
        $cupnIssStartDy = $_POST['cupnIssStartDy'];
        $cupnUseLmtDyYn = $_POST['cupnUseLmtDyYn'];
        $cuponcheck = $_POST['cuponcheck'];
        $dispCtgrNo = $_POST['dispCtgrNo'];
        $dispCtgrStatCd = $_POST['dispCtgrStatCd'];
        $dscAmtPercnt = $_POST['dscAmtPercnt'];
        $exchDlvCst = $_POST['exchDlvCst'];
        $htmlDetail = $_POST['htmlDetail'];
        $imageKindChk = $_POST['imageKindChk'];
        $minorSelCnYn = $_POST['minorSelCnYn'];
        $optWrtCnt = $_POST['optWrtCnt'];
        $orgnNmVal = $_POST['orgnNmVal'];
        $outsideYnIn = $_POST['outsideYnIn'];
        $outsideYnOut = $_POST['outsideYnOut'];
        $pointValue = $_POST['pointValue'];
        $pointYN = $_POST['pointYN'];
        $prdImage01 = $_POST['prdImage01'];
        $prdImage02 = $_POST['prdImage02'];
        $prdNm = $_POST['prdNm'];
        $prdSelQty = $_POST['prdSelQty'];
        $prdStatCd = $_POST['prdStatCd'];
        $prdTypCd = $_POST['prdTypCd'];
        $prdUpdYN = $_POST['prdUpdYN'];
        $prdWght = $_POST['prdWght'];
        $preSelPrc = $_POST['preSelPrc'];
        $addCompPrc = $_POST['addCompPrc'];
        $addPrdGrpNm = $_POST['addPrdGrpNm'];
        $addPrdWght = $_POST['addPrdWght'];
        $addUseYn = $_POST['addUseYn'];
        $compPrdNm = $_POST['compPrdNm'];
        $compPrdQty = $_POST['compPrdQty'];

        //2
        $addCompPrc = $_POST['addCompPrc2'];
        $addPrdGrpNm = $_POST['addPrdGrpNm2'];
        $addPrdWght = $_POST['addPrdWght2'];
        $addUseYn = $_POST['addUseYn2'];
        $compPrdNm = $_POST['compPrdNm2'];
        $compPrdQty = $_POST['compPrdQty2'];

        $colValue0 = $_POST['colValue0'];
        $proxyYn = $_POST['proxyYn'];
        $rootCtgrNo = $_POST['rootCtgrNo'];
        $rtngExchDetail = $_POST['rtngExchDetail'];
        $rtngdDlvCst = $_POST['rtngdDlvCst'];
        $selLimitQty = $_POST['selLimitQty'];
        $selLimitTypCd = $_POST['selLimitTypCd'];
        $selMinLimitQty = $_POST['selMinLimitQty'];
        $selMinLimitTypCd = $_POST['selMinLimitTypCd'];
        $selMthdCd = $_POST['selMthdCd'];
        $selPrc = $_POST['selPrc'];
        $sellerItemEventYn = $_POST['sellerItemEventYn'];
        $sellerPrdCd = $_POST['sellerPrdCd'];
        $shopNo = $_POST['shopNo'];
        $spplWyCd = $_POST['spplWyCd'];
        $suplDtyfrPrdClfCd = $_POST['suplDtyfrPrdClfCd'];
        $nResult = $_POST['nResult'];
        $dlvCndtSeq = $_POST['dlvCndtSeq'];
        $selTermUseYn = $_POST['selTermUseYn'];
        $selPrdClfCd = $_POST['selPrdClfCd'];
        $aplBgnDy = $_POST['aplBgnDy'];
        $aplEndDy = $_POST['aplEndDy'];
        
        $colTitle = $_POST['colTitle'];
        $optWght = $_POST['optWght'];
        $prdAttrCd = $_POST['prdAttrCd'];
        $colValue1 = $_POST['colValue1'];
        $colCount = $_POST['colCount'];
        $optStatus = $_POST['optStatus'];
        $optPrc = $_POST['optPrc'];
        $selMnbdNckNm = $_POST['selMnbdNckNm'];
		$dom = new DOMDocument('1.0','UTF-8');
        $dom->formatOutput = true;
        $root = $dom->createElement('Product');
        $dom->appendChild($root);
        $root->appendChild( $dom->createElement('advrtStmt', $advrtStmt) );
		$root->appendChild( $dom->createElement('selMthdCd', $selMthdCd) );
		$root->appendChild( $dom->createElement('asDetail', $asDetail) );
		$root->appendChild( $dom->createElement('prdTypCd', $prdTypCd) );
		$root->appendChild( $dom->createElement('prdNm', $prdNm) );
		$root->appendChild( $dom->createElement('prdStatCd', $prdStatCd) );
		$root->appendChild( $dom->createElement('dispCtgrNo', $dispCtgrNo) );
        $root->appendChild( $dom->createElement('bndlDlvCnYn',$bndlDlvCnYn) );
        $root->appendChild($dom->createElement('cupnDscMthdCd',$cupnDscMthdCd));
		$root->appendChild($dom->createElement('cupnIssEndDy',$cupnIssEndDy));
		$root->appendChild($dom->createElement('cupnIssStartDy',$cupnIssStartDy));
		$root->appendChild($dom->createElement('cupnUseLmtDyYn',$cupnUseLmtDyYn));
		$root->appendChild($dom->createElement('cuponcheck',$cuponcheck));
		$root->appendChild($dom->createElement('dispCtgrStatCd',$dispCtgrStatCd));
		$root->appendChild($dom->createElement('dscAmtPercnt',$dscAmtPercnt));
		$root->appendChild($dom->createElement('exchDlvCst',$exchDlvCst));
		$root->appendChild($dom->createElement('htmlDetail',$htmlDetail));
		$root->appendChild($dom->createElement('imageKindChk',$imageKindChk));
		$root->appendChild($dom->createElement('minorSelCnYn',$minorSelCnYn));
		$root->appendChild($dom->createElement('optWrtCnt',$optWrtCnt));
		$root->appendChild($dom->createElement('orgnNmVal',$orgnNmVal));
		$root->appendChild($dom->createElement('outsideYnIn',$outsideYnIn));
		$root->appendChild($dom->createElement('outsideYnOut',$outsideYnOut));
		$root->appendChild($dom->createElement('pointValue',$pointValue));
		$root->appendChild($dom->createElement('pointYN',$pointYN));
		$root->appendChild($dom->createElement('prdAttrCd',$prdAttrCd));
		$root->appendChild($dom->createElement('prdImage01',$prdImage01));
		$root->appendChild($dom->createElement('prdImage02',$prdImage02));
		$root->appendChild($dom->createElement('prdNm',$prdNm));
		$root->appendChild($dom->createElement('prdSelQty',$prdSelQty));
		$root->appendChild($dom->createElement('prdStatCd',$prdStatCd));
		$root->appendChild($dom->createElement('prdUpdYN',$prdUpdYN));
		$root->appendChild($dom->createElement('prdWght',$prdWght));
		$root->appendChild($dom->createElement('preSelPrc',$preSelPrc));
		//productSelOption
		$productSelOption = $root->appendChild($dom->createElement('productSelOption'));
		$productSelOption->appendChild($dom->createElement('colValue0',$colValue0));
		$productSelOption->appendChild($dom->createElement('colValue1',$colValue1));
		$productSelOption->appendChild($dom->createElement('colCount',$colCount));
		$productSelOption->appendChild($dom->createElement('addUseYn',$addUseYn));
		$productSelOption->appendChild($dom->createElement('optStatus',$optStatus));
		$productSelOption->appendChild($dom->createElement('optWght',$optWght));
		$productSelOption->appendChild($dom->createElement('optPrc',$optPrc));

		$ProductComponent = $root->appendChild($dom->createElement('ProductComponent'));
		$ProductComponent->appendChild($dom->createElement('addCompPrc',$addCompPrc));
		$ProductComponent->appendChild($dom->createElement('addPrdGrpNm',$addPrdGrpNm));
		$ProductComponent->appendChild($dom->createElement('addPrdWght',$addPrdWght));
		$ProductComponent->appendChild($dom->createElement('addUseYn',$addUseYn));
		$ProductComponent->appendChild($dom->createElement('compPrdNm',$compPrdNm));
		$ProductComponent->appendChild($dom->createElement('compPrdQty',$compPrdQty));
//2
		$ProductComponent = $root->appendChild($dom->createElement('ProductComponent'));
		#$ProductComponent->appendChild($dom->createElement('addCompPrc',$addCompPrc2));
	##	$ProductComponent->appendChild($dom->createElement('addPrdGrpNm',$addPrdGrpNm2));
	#	$ProductComponent->appendChild($dom->createElement('addPrdWght',$addPrdWght2));
	#	$ProductComponent->appendChild($dom->createElement('addUseYn',$addUseYn2));
	#	$ProductComponent->appendChild($dom->createElement('compPrdNm',$compPrdNm2));
	#	$ProductComponent->appendChild($dom->createElement('compPrdQty',$compPrdQty2));

		$productWrtOption = $root->appendChild($dom->createElement('productWrtOption'));
		$productWrtOption->appendChild($dom->createElement('colValue0',$colValue0));
		# $productWrtOption->appendChild($dom->createElement('colValue1',$colValue1));
		$root->appendChild($dom->createElement('colTitle',$colTitle));
		$root->appendChild($dom->createElement('proxyYn',$proxyYn));
		$root->appendChild($dom->createElement('rootCtgrNo',$rootCtgrNo));
		$root->appendChild($dom->createElement('rtngExchDetail',$rtngExchDetail));
		$root->appendChild($dom->createElement('rtngdDlvCst',$rtngdDlvCst));
		$root->appendChild($dom->createElement('selLimitQty',$selLimitQty));
		$root->appendChild($dom->createElement('selLimitTypCd',$selLimitTypCd));
		$root->appendChild($dom->createElement('selMinLimitQty',$selMinLimitQty));
		$root->appendChild($dom->createElement('selMinLimitTypCd',$selMinLimitTypCd));
		$root->appendChild($dom->createElement('selMnbdNckNm',$selMnbdNckNm));
		$root->appendChild($dom->createElement('selMthdCd',$selMthdCd));
		$root->appendChild($dom->createElement('selPrc',$selPrc));
		$root->appendChild($dom->createElement('sellerItemEventYn',$sellerItemEventYn));
		$root->appendChild($dom->createElement('sellerPrdCd',$sellerPrdCd));
		// $root->appendChild($dom->createElement('shopNo','0'));
		// $root->appendChild($dom->createElement('spplWyCd','02'));
		$root->appendChild($dom->createElement('suplDtyfrPrdClfCd',$suplDtyfrPrdClfCd));
		// $root->appendChild($dom->createElement('validateMsg',''));
		// $root->appendChild($dom->createElement('nResult','0'));
		$root->appendChild($dom->createElement('dlvCndtSeq',$dlvCndtSeq));
		$root->appendChild($dom->createElement('selTermUseYn',$selTermUseYn));
		$root->appendChild($dom->createElement('selPrdClfCd',$selPrdClfCd));
		$root->appendChild($dom->createElement('aplBgnDy',$aplBgnDy));
		$root->appendChild($dom->createElement('aplEndDy',$aplEndDy));
        echo $dataProduct =  $dom->saveXML();#die;
		 $dom->save(''.$shopId.'-'.'result.xml') or die('XML Create Error');
		 $curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "http://api.11street.co.th/rest/prodservices/product/",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $dataProduct,
		CURLOPT_HTTPHEADER => array(
			"AcceptCharset: utf8",
			"Cache-Control: no-cache",
			"Content-Type: application/xml",
			"openapikey: 50667854bb253d281ce0fe36ebaeebaa"
		),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		echo $response;
		}
	#	echo 'dssssssssssssss';
    }
    public function updateProduct(){

        $shopId = $_POST['shopId'];
        $shopname = 'tarad';
        $prdNo = '271761';
        $advrtStmt = $_POST['advrtStmt'];
        $asDetail = $_POST['asDetail'];
        $bndlDlvCnYn = $_POST['bndlDlvCnYn'];
        $cupnDscMthdCd = $_POST['cupnDscMthdCd'];
        $cupnIssEndDy = $_POST['cupnIssEndDy'];
        $cupnIssStartDy = $_POST['cupnIssStartDy'];
        $cupnUseLmtDyYn = $_POST['cupnUseLmtDyYn'];
        $cuponcheck = $_POST['cuponcheck'];
        $dispCtgrNo = $_POST['dispCtgrNo'];
        $dispCtgrStatCd = $_POST['dispCtgrStatCd'];
        $dscAmtPercnt = $_POST['dscAmtPercnt'];
        $exchDlvCst = $_POST['exchDlvCst'];
        $htmlDetail = $_POST['htmlDetail'];
        $imageKindChk = $_POST['imageKindChk'];
        $minorSelCnYn = $_POST['minorSelCnYn'];
        $optWrtCnt = $_POST['optWrtCnt'];
        $orgnNmVal = $_POST['orgnNmVal'];
        $outsideYnIn = $_POST['outsideYnIn'];
        $outsideYnOut = $_POST['outsideYnOut'];
        $pointValue = $_POST['pointValue'];
        $pointYN = $_POST['pointYN'];
        $prdImage01 = $_POST['prdImage01'];
        $prdImage02 = $_POST['prdImage02'];
        $prdNm = $_POST['prdNm'];
        $prdSelQty = $_POST['prdSelQty'];
        $prdStatCd = $_POST['prdStatCd'];
        $prdTypCd = $_POST['prdTypCd'];
        $prdUpdYN = $_POST['prdUpdYN'];
        $prdWght = $_POST['prdWght'];
        $preSelPrc = $_POST['preSelPrc'];
        $addCompPrc = $_POST['addCompPrc'];
        $addPrdGrpNm = $_POST['addPrdGrpNm'];
        $addPrdWght = $_POST['addPrdWght'];
        $addUseYn = $_POST['addUseYn'];
        $compPrdNm = $_POST['compPrdNm'];
        $compPrdQty = $_POST['compPrdQty'];

        //2
        $addCompPrc = $_POST['addCompPrc2'];
        $addPrdGrpNm = $_POST['addPrdGrpNm2'];
        $addPrdWght = $_POST['addPrdWght2'];
        $addUseYn = $_POST['addUseYn2'];
        $compPrdNm = $_POST['compPrdNm2'];
        $compPrdQty = $_POST['compPrdQty2'];

        $colValue0 = $_POST['colValue0'];
        $proxyYn = $_POST['proxyYn'];
        $rootCtgrNo = $_POST['rootCtgrNo'];
        $rtngExchDetail = $_POST['rtngExchDetail'];
        $rtngdDlvCst = $_POST['rtngdDlvCst'];
        $selLimitQty = $_POST['selLimitQty'];
        $selLimitTypCd = $_POST['selLimitTypCd'];
        $selMinLimitQty = $_POST['selMinLimitQty'];
        $selMinLimitTypCd = $_POST['selMinLimitTypCd'];
        $selMthdCd = $_POST['selMthdCd'];
        $selPrc = $_POST['selPrc'];
        $sellerItemEventYn = $_POST['sellerItemEventYn'];
        $sellerPrdCd = $_POST['sellerPrdCd'];
        $shopNo = $_POST['shopNo'];
        $spplWyCd = $_POST['spplWyCd'];
        $suplDtyfrPrdClfCd = $_POST['suplDtyfrPrdClfCd'];
        $nResult = $_POST['nResult'];
        $dlvCndtSeq = $_POST['dlvCndtSeq'];
        $selTermUseYn = $_POST['selTermUseYn'];
        $selPrdClfCd = $_POST['selPrdClfCd'];
        $aplBgnDy = $_POST['aplBgnDy'];
        $aplEndDy = $_POST['aplEndDy'];
        
        $colTitle = $_POST['colTitle'];
        $optWght = $_POST['optWght'];
        $prdAttrCd = $_POST['prdAttrCd'];
        $colValue1 = $_POST['colValue1'];
        $colCount = $_POST['colCount'];
        $optStatus = $_POST['optStatus'];
        $optPrc = $_POST['optPrc'];
        $selMnbdNckNm = $_POST['selMnbdNckNm'];

        $simple_xml = new SimpleXMLElement('<Product></Product>');

        // $simple_xml->addChild('result');
        // $simple_xml->addAttribute('id', 1);
      
        $simple_xml->addChild('advrtStmt', $advrtStmt);
        $simple_xml->addChild('asDetail', $asDetail);
        $simple_xml->addChild('bndlDlvCnYn', $bndlDlvCnYn);
        $simple_xml->addChild('cupnDscMthdCd', $cupnDscMthdCd);
        $simple_xml->addChild('cupnIssEndDy', $cupnIssEndDy);
        $simple_xml->addChild('cupnIssStartDy', $cupnIssStartDy);
        $simple_xml->addChild('cupnUseLmtDyYn', $cupnUseLmtDyYn);
        $simple_xml->addChild('cuponcheck', $cuponcheck);
        $simple_xml->addChild('dispCtgrNo', $dispCtgrNo);
        $simple_xml->addChild('dispCtgrStatCd', $dispCtgrStatCd);
        $simple_xml->addChild('dscAmtPercnt', $dscAmtPercnt);
        $simple_xml->addChild('exchDlvCst', $exchDlvCst);
        $simple_xml->addChild('htmlDetail', $htmlDetail);
        $simple_xml->addChild('imageKindChk', $imageKindChk);
        $simple_xml->addChild('minorSelCnYn', $minorSelCnYn);
        $simple_xml->addChild('optWrtCnt', $optWrtCnt);
        $simple_xml->addChild('orgnNmVal', $orgnNmVal);
        $simple_xml->addChild('outsideYnIn', $outsideYnIn);
        $simple_xml->addChild('outsideYnOut', $outsideYnOut);
        $simple_xml->addChild('pointValue', $pointValue);
        $simple_xml->addChild('pointYN', $pointYN);
        $simple_xml->addChild('prdImage01', $prdImage01);
        $simple_xml->addChild('prdImage02', $prdImage02);
        $simple_xml->addChild('prdNm', $prdNm);
        $simple_xml->addChild('prdSelQty', $prdSelQty);
        $simple_xml->addChild('prdStatCd', $prdStatCd);
        $simple_xml->addChild('prdTypCd', $prdTypCd);
        $simple_xml->addChild('prdUpdYN', $prdUpdYN);
        $simple_xml->addChild('prdWght', $prdWght);
        $simple_xml->addChild('preSelPrc', $preSelPrc);

        // ProductComponent

        $simple_xml->addChild('ProductComponent');
        $simple_xml->ProductComponent[0]->addChild('addCompPrc', $addCompPrc);
        $simple_xml->ProductComponent[0]->addChild('addPrdGrpNm', $addPrdGrpNm);
        $simple_xml->ProductComponent[0]->addChild('addPrdWght', $addPrdWght);
        $simple_xml->ProductComponent[0]->addChild('addUseYn', $addUseYn);
        $simple_xml->ProductComponent[0]->addChild('compPrdNm', $compPrdNm);
        $simple_xml->ProductComponent[0]->addChild('compPrdQty', $compPrdQty);

        $simple_xml->addChild('productWrtOption');
        $simple_xml->productWrtOption[0]->addChild('colValue0', $colValue0);

        $simple_xml->addChild('rootCtgrNo', $rootCtgrNo);
        $simple_xml->addChild('rtngExchDetail', $rtngExchDetail);
        $simple_xml->addChild('rtngdDlvCst', $rtngdDlvCst);
        $simple_xml->addChild('selLimitQty', $selLimitQty);
        $simple_xml->addChild('selLimitTypCd', $selLimitTypCd);
        $simple_xml->addChild('selMthdCd', $selMthdCd);
        $simple_xml->addChild('selPrc', $selPrc);
        $simple_xml->addChild('sellerItemEventYn', $sellerItemEventYn);
        $simple_xml->addChild('sellerPrdCd', $sellerPrdCd);
        $simple_xml->addChild('shopNo', $shopNo);
        $simple_xml->addChild('spplWyCd', $spplWyCd);
        $simple_xml->addChild('suplDtyfrPrdClfCd', $suplDtyfrPrdClfCd);
        $simple_xml->addChild('nResult', $nResult);
        $simple_xml->addChild('dlvCndtSeq', $dlvCndtSeq);
        $simple_xml->addChild('selTermUseYn', $selTermUseYn);
        $simple_xml->addChild('selPrdClfCd', $selPrdClfCd);
        $simple_xml->addChild('aplBgnDy', $aplBgnDy);
        $simple_xml->addChild('aplBgnDy', $aplBgnDy);
        $simple_xml->asXML(''.$shopId.'-'.$shopname.'.xml') or die('XML Update Error');

        if (!is_dir('streettest/'.$shopId.'-'.$shopname)) {
           $save = mkdir('./streettest/' . $shopId.'-'.$shopname, 0777, TRUE);
           if($save){
               echo 'save';
           }else{
               echo 'error';
           }

        }
        // $curl = curl_init();
		// curl_setopt_array($curl, array(
		// CURLOPT_URL => "http://api.11street.co.th/rest/prodservices/product/".$prdNo,
		// CURLOPT_RETURNTRANSFER => true,
		// CURLOPT_ENCODING => "",
		// CURLOPT_MAXREDIRS => 10,
		// CURLOPT_TIMEOUT => 30,
		// CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		// CURLOPT_CUSTOMREQUEST => "POST",
		// CURLOPT_POSTFIELDS => $simple_xml2,
		// CURLOPT_HTTPHEADER => array(
		// 	"AcceptCharset: utf8",
		// 	"Cache-Control: no-cache",
		// 	"Content-Type: application/xml",
		// 	"openapikey: 50667854bb253d281ce0fe36ebaeebaa"
		// ),
		// ));
		// $response = curl_exec($curl);
		// $err = curl_error($curl);

		// curl_close($curl);

		// if ($err) {
		// echo "cURL Error #:" . $err;
		// } else {
		// echo $response;
		// }
    }
}  