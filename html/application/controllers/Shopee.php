<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shopee extends CI_Controller {
    private $openapikey = 'b37c061daf2fcfa2baffe7539110938be5b7525041c147e78ad8afa78cc1a72d';
    private $url = 'https://partner.shopeemobile.com/api/v1/';
    private $header;
    private $product;
    public function __construct(){
        parent::__construct();
        $this->load->library('restclient');
        $this->header = array(
            "Host"=> $this->url,
            "Content-Type"=>"appliction/่json",
            "AcceptCharset"=>"utf8",
            "Content-Lenght"=>"89",
            "openapikey"=>$this->openapikey
        );
        #$this->load->model('product_model');
    }
    public function index(){
        $this->restclient->debug();
    }
    public function addItem(){
        $data = array(
            'name' => $this->input->post('name'),
            'description'=> $this->input->post('description'),
            'price' => $this->input->post('price'),
            'stock' => $this->input->post('stock'),
            'item_sku' => $this->input->post('item_sku'),
            'variations'=> $this->input->post('variations'),
            'name_2' => $this->input->post('name_2'),
            'stock_2' => $this->input->post('stock_2'),
            'variation_sku'=> $this->input->post('vatiation_sku'),
            'images'=> $this->input->post('images'),
            'url' => $this->input->post('url'),
            'attributes'=> array(
               'attributes_id' => $this->input->post('attributes_id'), 
               'value' => $this->input->post('value')
            ),
            'logistics' => array(
                'logistics_id' => $this->input->post('logistics_id'),
                'enabled' => $this->input->post('enabled'),
                'shipping_fee' => $this->input->post('shipping_fee'),
                'size_id' => $this->input->post('size_id'),
                'is_free' => $this->input->post('is_free')
            ),
            'weight' => $this->input->post('weight'),
            'package_length' => $this->input->post('package_length'),
            'package_width' => $this->input->post('package_width'),
            'package_height' => $this->input->post('package_height'),
            'days_to_ship' => $this->input->post('days_to_ship'),
            'wholesales' => array(
                'min' => $this->input->post('min'),
                'max' => $this->input->post('max'),
                'unit_price' => $this->input->post('unit_price'), 
            ),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
            $url = $this->url."item/add";
            $result = $this->restclient->post($url,$this->header,$data);
            header('Content-Type:application/json');
            echo $result;
    }
    public function addItemImg(){
        $data = array(
            'item_id' => $this->input->post('item_id'),
            'images' => $this->input->post('images'),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
        $url = $this->url."item/img/add";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function addVariations(){
        $data = array(
            'item_id' => $this->input->post('item_id'),
            'variations' => array(
                'name' => $this->input->post('name'),
                'stock' => $this->input->post('stock'),
                'price' => $this->input->post('price'),
                'variations_sku' => $this->input->post('variations_sku')
            ),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
        $url = $this->url."item/add_variations";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function deleteItem(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        ); 
        $url = $this->url."item/delete";
        $result = $this->restclient->post($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function deleteItemImg(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'images' => $this->input->post('images'),
            'positions' => $this->input->post('postions'),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
        $url = $this->url."item/img/delete";
       // $result $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function deleteVariation(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'variation_id' => $this->input->post('variation_id'),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
        $url = $this->url."item/delete_variation";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function getAttributes(){
        $url = $this->url."item/attributes/get/";
        $result = $this->restclient->get($url,$this->header);
        if(!$result)die($this->restclient->debug());
        header("Content-Type:appliction/่json");
        echo $result;
    }
    public function getCategories(){
        $url = $this->url."item/categories/get/";
        $result = $this->restclient->get($url,$this->header);
        if(!$result)die($this->restclient->debug());
        header("Content-Type:appliction/json");
        echo $result;
    }
    public function getItemDetail(){
        $url = $this->url."item/get/";
        $result = $this->restclient->get($url,$this->header);
        if(!$result)die($this->restclient->debug());
        header("Content-Type:appliction/json");
        echo $result;
    }
    public function getItemList(){
        $url = $this->url."item/get";
        $result = $this->restclient->get($url,$this->header);
        if(!$result)die($this->restclient->debug());
        header("Content-Type:application/json");
        echo $result;
    }
    public function insertItemImg(){
        $item_id = $this->input->post('item_id');
        $data =  array(
            'image_url'=> $this->input->post('image_url'),
            'image_position' => $this->input->post('image_position'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => date('Y:M:d H:i:s')
        );
        $url = $this->url."item/img/insert";
        $result = $this->resetclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function updateItem(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'category_id' => $this->input->post('category_id'),
            'name'  => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'item_sku'  => $this->input->post('item_sku'),
            'variations'=> array(
                'variations_id' => $this->input->post('variations_id'),
                'name' => $this->input->post('name_2'),
                'variation_sku' => $this->input->post('variation_sku'),
            ),
            'attributes' => array(
                'attributes_id' => $this->input->post('attributes_id'),
                'value' => $this->input->post('value'),
            ),
            'days_to_ship' => $this->input->post('days_to_ship'),
            'wholesales' => array(
                'min' => $this->input->post('min'),
                'max' => $this->input->post('max'),
                'unit_price' => $this->input->post('unit_price')
            ),
            'logistics' => array(
                'logistic_id' => $this->input->post('logistic_id'),
                'enabled' => $this->input->post('enabled'),
                'shipping_fee' => $this->input->post('shipping_fee'),
                'size_id' => $this->input->post('size_id'),
                'is_free' => $this->input->post('is_free'),
            ),
            'weight' => $this->input->post('weight'),
            'package_length' => $this->input->post('package_length'),
            'package_width' => $this->input->post('package_width'),
            'package_height' => $this->input->post('package_height'),
            'partner_id' => $this->input->post('paerner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => date("Y:m:d H:i:s")
        );
        $url = $this->url."item/update";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function updatePrice(){
        $data = array(
            'item_id'   => $this->input->post('item_id'),
            'price'     => $this->input->post('price'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."itemes/update_price";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function updateStock(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'stock' => $this->input->post('stock'),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
        $url = $this->url."items/update_stock";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function updateVariationPrice(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'variation_id' => $this->input->post('variation_id'),
            'price' => $this->input->post('price'),
            'partner_id' => $this->input->post('partner_id'),
            'shopid' => $this->input->post('shopid'),
            'timestamp' => date('Y:m:d H:i:s')
        );
        $url = $this->url."items/update_variation_price";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function updateVariationStock(){
        $item_id = $this->input->post('item_id');
        $data = array(
            'variation_id' => $this->input->post('variation_id'),
            'stock' => $this->input->post('stock'),
            'partner_id' => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => date("Y:m:d H:i:s")
        );
        $url = $this->url."items/update_variation_stock";
        $result = $this->resrtclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    ////////////////////////////////////////////////////////
    //  งง งง งง งง งง ง งง ง 
    // งง งง งง งง งง งง
    // งง งง งง
    // งง
    // งง
    // งง
    public function logisticsGetAddress(){
        $partner_id = $this->input->get('partner_id');
        $shopid = $this->input->get('shopid');
        $timestamp =$this->input->post('timestamp');
        //
		$url = $this->url."logistics/address/get";
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/json');
		echo $result;
    }
    public function logisticsGetAirwayBill(){
        $ordersn_list = $this->input->post('ordersn_list');
        $is_batch = $this->input->post('is_batch');
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');
        //
        $url = $this->url."logistics/airway_bill/get_mass/";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type: application/json');
        echo $result;
    }
    public function logisticsGetBranch(){
        $ordersn  = $this->input->post('ordersn');
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');
        //
        $url = $this->url."logistics/branch/get/";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function logisticsGetLogistics(){
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');
        //
        $url = $this->url."logistics/channel/get";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type: application/json');
        echo $result;
    }
    public function logisticsGetLogisticsMessage(){
        $ordersn = $this->input->post('ordersn');
        $tracking_number = $this->input->post('tracking_number');
        $partner_id  = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');

        //
        $url = $this->url."logistics/tracking";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function logisticsGetOrderLogistics(){
        $ordersn = $this->input->post('ordersn');
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');
        //
        $url = $this->url."logistics/order/get";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function logisticsGetParameterForInit(){
        $ordersn = $this->input->post('ordersn');
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');
        //
        $url = $this->url."logistics/init_parameter/get";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function logisticsGetTimeSlot(){
        $ordersn = $this->input->post('ordersn');
        $address_id = $this->input->post('address_id');
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        //
        $url = $this->url."/ogistics/timeslot/get";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function logisticsGetTrackgNo(){
        $ordersn_list = $this->input->post('ordersn_list');
        $partner_id = $this->input->post('partner_id');
        $shopid = $this->input->post('shopid');
        $timestamp = $this->input->post('timestamp');
        //
        $url = $this->url."logistics/tracking_number/get_mass";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $result;
    }
    public function logisticsGetInit(){
        $data = array(
            'ordersn' => $this->input->post('ordersn'),
            'pickup' => array(
                'address_id' => $this->input->post('addres_id'),
                'pickup_time_id' => $this->input->post('pickup_time_id'),
                'tracking_no'   => $this->input->post('tracking_no')
            ),
            'dropoff'   => array(
                'branch_id' => $this->input->post('branch_id'),
                'sender_real_name'  => $this->input->post('sender_real_name'),
                'tracking_no'   => $this->input->post('tracking_no')
            ),
            'non_integrated'    => array(
                'tracking_no'   => $this->input->post('tracking_no')
            ),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."logistics/init";
        $resutl = $this->restclient->get($url,$this->header);
        header('Content-Type/application/json');
        echo $result;
    }
    public function logisticsGetSetLogisticStatus(){
        $data = array(
            'ordersn'   => $this->input->post('ordersn'),
            'tracking_number'   => $this->input->post('tracking_number'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."logistics/offline/set";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $resutl;
    }
    public function logisticsGetSetTrackingNo(){
        $data = array(
            'info_list' => array(
                'ordersn' => $this->input->post('ordersn'),
                'tracking_number' => $this->input->post('tracking_number'),
            ),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url  = $this->url."/logistics/tracking_number/set_mass";
        $result = $this->restclient->get($url,$this->header);
        header('Content-Type:application/json');
        echo $resutl;
    }
    // post Order shopee 
    public function getEscrowDetail(){
        $data = array(
            'ordersn' => $this->input->post('ordersn'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/my_income/";
        $resutl = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function getOrderDeatail(){
        $data = array(
            'ordersn_list' => $this->input->post('ordersn_list'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/detail";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function getOrderByStatus(){
        $data = array(
            'order_status'  => $this->input->post('order_status'),
            'create_time_from' => $this->input->post('create_time_from'),
            'create_time_to'    => $this->input->post('create_time_to'),
            'pagiantion_entries_per_page'   => $this->input->post('pagiantion_entries_per_page'),
            'pagination_offset' => $this->input->post('pagiation_offset'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."order/get";
        $rsult = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function getOrderList(){
        $data = array(
            'create_time_from'  => $this->input->post('create_time_from'),
            'create_time_to'    => $this->input->post('create_time_to'),
            'update_time_from'  => $this->input->post('update_time_from'),
            'pagination_entries_per_page'   => $this->input->post('pagination_entries_per_page'),
            'pagiation_offset'  => $this->input->post('pagiation_offset'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/basics";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function AcceptBuyerCancellation(){
        $data = array(
            'ordersn'   => $this->input->post('ordersn'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/buyer_cancellation/acceap";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-type:application/json');
        echo $result;
    }
    public function AddOrderNote(){
        $data = array(
            'ordersn'   => $this->input->post('ordersn'),
            'note'  => $this->input->post('note'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/note/add";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function CancelOrder(){
        $data = array(
            'ordersn'   => $this->input->post('ordersn'),
            'cancel_reason' => $this->input->post('cancel_reason'),
            'item_id'   => $this->input->post('item_id'),
            'variation_id'  => $this->input->post('variation_id'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/cancel/";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function RejectBuyerCancellation(){
        $data = array(
            'ordersn'   => $this->input->post('ordersn'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."orders/buyer_cancellation/reject";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    // returns
    public function ConfirmReturn(){
        $data = array(
            'returnsn'  => $this->input->post('returns'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."returns/confirm";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    } 
    public function DisputeReturn(){
        $data = array(
            'returnsn'  => $this->input->post('returnsn'),
            'email' => $this->input->post('email'),
            'dispute_reason'    => $this->input->post('dispute_reason'),
            'dispute_text_reason'   => $this->input->post('dispute_text_reason'),
            'images'    => $this->input->post('images'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timesamp')
        );
        $url = $this->url."returns/dispute";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-type:application/json');
        echo $result;
    }
    public function getReturnList(){
        $data = array(
            'pagination_offset' => $this->input->post('pagination_offset'),
            'pagination_entries_per_page'   => $this->input->post('pagination_entries_per_page'),
            'create_time_from'  => $this->input->post('create_time_from'),
            'create_time_to'    => $this->input->post('create_time_to'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."returns/get";
        $result = $this->restclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    //////////////////////////////// shop
    //post
    public function getShopInfo(){
        $data = array(
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp'  => $this->input->post('timestamp'),
        );
        $url = $this->url."shop/get/";
        $result = $this->resetclient->post($url,$this->header,$data);
        header('Content-Type:application/json');
        echo $result;
    }
    public function UpdateShopInfo(){
        $data = array(
            'shop_name' => $this->input->post('shop_name'),
            'images'    => $this->input->post('images'),
            'videos'    => $this->input->post('videos'),
            'disable_make_offer'    => $this->input->post('disable_make_offer'),
            'enable_display_unitno' => $this->input->post('enable_display_unitno'),
            'shop_description'  => $this->input->post('shop_description'),
            'partner_id'    => $this->input->post('partner_id'),
            'shopid'    => $this->input->post('shopid'),
            'timestamp' => $this->input->post('timestamp')
        );
        $url = $this->url."shop/update";
        $result = $this->resetclient->post($url,$this->header,$data);
        echo $result;
    }

}