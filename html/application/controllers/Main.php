<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends CI_Controller {
	private $openapikey = '50667854bb253d281ce0fe36ebaeebaa';
	private $url = 'http://api.11street.co.th/rest';
	private $header;
	private $product;
	public function __construct(){
		parent:: __construct();
		$this->load->library('restclient');
		$this->header   = array(
			"Contenttype"=>"application/xml",
			"AcceptCharset"=>"utf8",
			"openapikey"=>$this->openapikey
		);
		$this->load->model('product_model');
		/*$this->product = array(
			"prdNo"=>"",
			"selMthdCd"=>"",
			"dispCtgrNo"=>"",
			"prdStatCd"=>"",
			"prdImage01"=>"",
			"prdImage02"=>"",
			"prdImage03"=>"",
			"prdImage04"=>"",
			"prdImage05"=>"",
			"prdImage06"=>"",
			"prdImage07"=>"",
			"prdImage08"=>"",
			"htmlDetail"=>"",
			"orgnTypCd"=>"",
			"orgnTypDtlsCd"=>"",
			"sellerPrdCd"=>"",
			"selTermUseYn"=>"",
			"selPrdClfCd"=>"",
			"aplBgnDy"=>"",
			"aplEndDy"=>"",
			"wrhsPlnDy"=>"",
			"selPrc"=>"",
			"prdSelQty"=>"",
			"cuponcheck"=>"",
			"dscAmtPercnt"=>"",
			"cupnDscMthdCd"=>"",
			"cupnUseLmtDyYn"=>"",
			"cupnIssStartDy"=>"",
			"cupnIssEndDy"=>"",
			"optWrtCnt"=>"",
			"colValue0"=>"",
			"colValue1"=>"",
			"optSelCnt"=>"",
			"optionAllAddWght"=>"",
			"colTitle"=>"",
			"productSelOption"=>"",
			"colValue0"=>"",
			"colValue1"=>"",
			"colCount"=>"",
			"optStatus"=>"",
			"optWght"=>"",
			"ProductComponent"=>"",
			"addPrdGrpNm"=>"",
			"compPrdNm"=>"",
			"addCompPrc"=>"",
			"compPrdQty"=>"",
			"addUseYn"=>"",
			"addPrdWght"=>"",
			"selMinLimitQty"=>"",
			"asDetail"=>"",
			"dlvCndtSeq"=>"",
			"rtngExchDetail"=>"",
			"suplDtyfrPrdClfCd"=>"",
			"createCd"=>""
		)*/
		
	}
	public function index(){
		$this->restclient->debug();
	}
	public function category(){
		$url = $this->url."/cateservice/category";
		$result = $this->restclient->get($url, $this->header);
		if(!$result)die($this->restclient->debug());
		header('Content-Type: application/xml');
		echo $result;
	}	
	public function subCategoryByParentId($parent_id){
		$url = $this->url."/cateservice/category/".$parent_id;
		$result = $this->restclient->get($url,$header);
		header('Content-Type: application/xml');
		echo $result;		
	}
	public function InsertProduct(){ // insert.
		$data = array(
			'selMthaCd' => $this->input->post('selMthaCd'),
			'dispCtgNo' => $this->input->post('dispCtgNo'),
			'prdTypCd'  => $this->input->post('prdTypCd'),
			'prdNm'     => $this->input->post('prdNm'),
			'prdStatCd' => $this->input->post('prdStatCd'),
			'prdWght'	=> $this->input->post('prdWght'),
			'prdlmage01' => $this->input->post('prdlmage01'),
			'prdlmage02' => $this->input->post('prdlmage02'),
			'prdlmage03' => $this->input->post('prdlmage03'),
			'prdlmage04' => $this->input->post('prdlmage04'),
			'htmlDetail' => $this->input->post('htmlDetail'),
			'orngTypCd'  => $this->input->post('orngTypCd'),
			'orngTypDtlsCd' => $this->input->post('orngTypDtlsCd'),
			'sellerPrdCd'   => $this->input->post('sellerPrdCd'),
			'selTermUseYu'  => $this->input->post('sellerPrdCd'),
			'selPrdClfCd'	=> $this->input->post('selPrdClfCd'),
			'apiBgnDy'		=> $this->input->post('apibgnDy'),
			'apiEndDy'		=> $this->input->post('apiEndDy'),
			'wehsPlnDy'		=> $this->input->post('wehsPlnDy'),
			'selPrc'		=> $this->input->post('selPrc'),
			'prdSelQty'		=> $this->input->post('prdSelQty'),
			'cuponcheck'	=> $this->input->post('cuponcheck'),
			'dscAmtPercnt'	=> $this->input->post('dscAmtPercnt'),
			'cupnDscMthdCd'	=> $this->input->post('cupnDscMthdCd'),
			'cupnUseLmDyYn'	=> $this->input->post('cupnUseLmDyYn'),
			'cupnlssStarDy' => $this->input->post('cupnlssStarDy'),
			'cupnlssEndDy'	=> $this->input->post('cupnlssEndDy'),
			'optWrtCnt'		=> $this->input->post('optWrtCnt'),
			'productWrtOption' => $this->input->post('productWrtOption'),
			'optSelCnt'		   => $this->input->post('optSelCnt'),
			'optionAllQty'	=> $this->input->post('optionAllQty'),
			'optionAllAddWght'	=> $this->input->post('optionAllAddWght'),
			'optionAllAddPrc'	=> $this->input->post('optionAllAddPrc'),
			'colTitle'	=> $this->input->post('colTitle'),
			'productSelOtion'	=> $this->input->post('productSelOtion'),
			'optStatus'	=> $this->input->post('optStatus'),
			'optWght'	=> $this->input->post('optWght'),
			'optPrc'	=> $this->input->post('optPrc'),
			'ProductComponent'	=> $this->input->post('ProductComponent'),
			'addPrdGrpNm'		=> $this->input->post('addPrdGrpNm'),
			'compPrdNm'	=> $this->input->post('compPrdNm'),
			'addCompPrc' => $this->input->post('addCompPrc'),
			'compPrdQty'	=> $this->input->post('compPrdQty'),
			'addUseYn'	=> $this->input->post('addUseYn'),
			'addPrdWght'	=> $this->input->post('addPrdWght'),
			'selMinLimitQty' 	=> $this->input->post('selMinLimitQty'),
			'seLimitTypCd'	=> $this->input->post('seLimitTypCd'),
			'selLimitQty'	=> $this->input->post('selLimitQty'),
			'asDetail'	=> $this->input->post('asDetail'),
			'dlvCndtSeq'	=> $this->input->post('dlvCndtSeq'),
			'rtngExchDetail'	=> $this->input->post('rtngExchDetail'),
			'supIDtyfrPrdClfCd'	=> $this->input->post('supIDtyfrPrdClfCd'),
			'createCd'	=> $this->input->post('createCd'),
		);	
		$url = $this->url."prodservices/product";
		$result = $this->restclient->post($url,$data,$this->header);
		header('Content-Type: application/xml');
		echo $result;
		$this->restclient->debug();
	}
	public function EditProduct(){ // update
		$prdNo = $this->input->post('proNo');
		$data = array(
			'selMthaCd' => $this->input->post('selMthaCd'),
			'dispCtgNo' => $this->input->post('dispCtgNo'),
			'prdTypCd'  => $this->input->post('prdTypCd'),
			'prdNm'     => $this->input->post('prdNm'),
			'prdStatCd' => $this->input->post('prdStatCd'),
			'prdWght'	=> $this->input->post('prdWght'),
			'prdlmage01' => $this->input->post('prdlmage01'),
			'prdlmage02' => $this->input->post('prdlmage02'),
			'prdlmage03' => $this->input->post('prdlmage03'),
			'prdlmage04' => $this->input->post('prdlmage04'),
			'htmlDetail' => $this->input->post('htmlDetail'),
			'orngTypCd'  => $this->input->post('orngTypCd'),
			'orngTypDtlsCd' => $this->input->post('orngTypDtlsCd'),
			'sellerPrdCd'   => $this->input->post('sellerPrdCd'),
			'selTermUseYu'  => $this->input->post('sellerPrdCd'),
			'selPrdClfCd'	=> $this->input->post('selPrdClfCd'),
			'apiBgnDy'		=> $this->input->post('apibgnDy'),
			'apiEndDy'		=> $this->input->post('apiEndDy'),
			'wehsPlnDy'		=> $this->input->post('wehsPlnDy'),
			'selPrc'		=> $this->input->post('selPrc'),
			'prdSelQty'		=> $this->input->post('prdSelQty'),
			'cuponcheck'	=> $this->input->post('cuponcheck'),
			'dscAmtPercnt'	=> $this->input->post('dscAmtPercnt'),
			'cupnDscMthdCd'	=> $this->input->post('cupnDscMthdCd'),
			'cupnUseLmDyYn'	=> $this->input->post('cupnUseLmDyYn'),
			'cupnlssStarDy' => $this->input->post('cupnlssStarDy'),
			'cupnlssEndDy'	=> $this->input->post('cupnlssEndDy'),
			'optWrtCnt'		=> $this->input->post('optWrtCnt'),
			'productWrtOption' => $this->input->post('productWrtOption'),
			'optSelCnt'		   => $this->input->post('optSelCnt'),
			'optionAllQty'	=> $this->input->post('optionAllQty'),
			'optionAllAddWght'	=> $this->input->post('optionAllAddWght'),
			'optionAllAddPrc'	=> $this->input->post('optionAllAddPrc'),
			'colTitle'	=> $this->input->post('colTitle'),
			'productSelOtion'	=> $this->input->post('productSelOtion'),
			'optStatus'	=> $this->input->post('optStatus'),
			'optWght'	=> $this->input->post('optWght'),
			'optPrc'	=> $this->input->post('optPrc'),
			'ProductComponent'	=> $this->input->post('ProductComponent'),
			'addPrdGrpNm'		=> $this->input->post('addPrdGrpNm'),
			'compPrdNm'	=> $this->input->post('compPrdNm'),
			'addCompPrc' => $this->input->post('addCompPrc'),
			'compPrdQty'	=> $this->input->post('compPrdQty'),
			'addUseYn'	=> $this->input->post('addUseYn'),
			'addPrdWght'	=> $this->input->post('addPrdWght'),
			'selMinLimitQty' 	=> $this->input->post('selMinLimitQty'),
			'seLimitTypCd'	=> $this->input->post('seLimitTypCd'),
			'selLimitQty'	=> $this->input->post('selLimitQty'),
			'asDetail'	=> $this->input->post('asDetail'),
			'dlvCndtSeq'	=> $this->input->post('dlvCndtSeq'),
			'rtngExchDetail'	=> $this->input->post('rtngExchDetail'),
			'supIDtyfrPrdClfCd'	=> $this->input->post('supIDtyfrPrdClfCd'),
			'createCd'	=> $this->input->post('createCd'),
		);
		echo $t = $this->header;die;
		$url = $this->url."/prodservices/updateProductOption/".$prdNo;
		$result = $this->restclient->put($url,$this->header,$data);
		header('Content-Type: application/xml');
		echo $result;
	}

	public function EditItemPrice(){
		$prdNo = $this->input->post('prdNo');
		$selPrc = $this->input->post('selPrc');
		$url = $this->url."/prodservices/product/price/".$proNo.'/'.$selPrc;
		// $ch = curl_init($url);
		// curl_setopt($ch , CURLOPT_CUSTOMREQUEST,"PUT");
		// curl_setopt($ch , CURLOPT_POSTFIELDS, http_build_query($data));
		// curl_setopt($ch , CURLOPT_RETURNTRANSFER , true);
		// $response_json = curl_exec($ch);
		// $result = json_decode($response_json,true);
	}
	public function EditItemDiscount(){
		$proNo = $this->input->post('proNo');
		$data = array(
			'selPrc' => $this->input->post('selPrc'),
			'cuponcheck' => $this->input->post('cuponcheck'),
			'dscAmtPercnt' => $this->input->post('dscAmtPercnt'),
			'cupnDscMthdCd' => $this->input->post('cupnDscMthdCd'),
			'cupnUseLmtDyYn' => $this->input->post('cupnUseLmtDyYn'),
			'cupnlssStarDy'	=> $this->input->post('cupnlssStarDy'),
			'cupnlssEndDy' 	=> $this->input->post('cupnlssEndDy'),
		);
		$url = $this->url."/prodservices/product/priceCoupon/".$proNo;
		$result = $this->restclient->put($url,$this->header,$data);
		header('Content-Type: application/xml');
		echo $result;
	}
	public function UpdateStock(){
		$prdStackNo = $this->input->post('prdStackNo');
		$data = array(
			'prdNo' => $this->input->post('prdNo'),
			'stockQty' => $this->input->post('stockQty')
		);
		$url = $this->url."/prodservices/stockqty/".$prdStackNo;
		$result = $this->restclient->put($url,$this->header,$data);
		header('Content-Type: application/xml');
		echo $result;
	}
	public function StatStopDisplay(){
		$prdNo = $this->input->post('prdNo');
		$url = $this->url."/prodstatservice/stat/stopdisplay/".$prdNo;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;
	}
	public function StatRestartDisplay(){
		$prdNo = $this->input->post('prdNo');
		$url = $this->url."/prodstatservice/stat/restartdisplay/".$prdNo;
	}
	public function ProductStockNo(){
		$prdNo = $this->input->get('prdNo');
		$url = $this->url."/prodmarketservice/prodmarket/stck/".$prdNo;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;
	}
	public function InsertProductStockNo(){
		$prdNo = $this->input->post('prdNo');
		$url = $this->url."/prodmarketservice/prodmarket/stocks";
	}
	public function InsertProductStockNoFull(){
		$prdNo = $this->input->post('prdNo');
		$data = array(
			'addPrc' => $this->input->post('addPrc'),
			'mixDtlOptNm' => $this->input->post('mixDtlOptNm'),
			'mixOptNm' => $this->input->post('mixOptNm'),
			'mixOptNo' => $this->input->post('mixOptNo'),
			'optWght'	=> $this->input->post('optWght'),
			'prdNo'	=> $this->input->post('prdNo'),
			'ordStckStatCd' => $this->input->post('ordStckStatCd'),
			'selQty'	=> $this->input->post('selQty'),
			'stcakQty'	=> $this->input->post('stcakQty'),
			'prdNm'	=> $this->input->post('prdNm'),
			'sellerPrdCd' => $this->input->post('sellerPrdCd'),
			'message'	=> $this->input->post('message'),
		);
		$url = $this->url."/prodmarketservice/prodmarket/stocks/";
		$result = $this->restclient->post($url,$this->header,$data);
		header('Content-Type: application/xml');
		echo $result;
	}
	public function getProductView($prdNo){
		$url = $this->url."/prodservices/product/details/".$prdNo;
		$result = $this->restclient->get($url,$header);
		header('Content-Type: application/xml');
		echo $result;		
	}
	public function getOrder(){
	//	$id = $this->url->segment(3);

		$startTime = $this->input->post('startTime');
		$endTime = $this->input->post('endTime');
		$url = $this->url."/ordservices/complete/".$startTime."/".$endTime;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;	
	}
	public function getOrderConfirmationProcess(){
		//$id = $this->url->segment(3);
		$ordNo = $this->input->get('ordNo');
		$ordPrdSeq = $this->input->get('ordPrdSeq');
		$url = $this->url."ordservices/reqpackaging/".$ordNo."/".$ordPrdSeq;
		$result = $this->restclient->get($url,$header);
		header('Content-Type: application/xml');
		echo $result;	
	}
	public function OrderSToBeProcessed(){
		$startTime = $this->input->get('startTime');
		$endTime	= $this->input->get('endTime');
		$url = $this->url."/ordservices/packaging/".$startTime."/".$endTime;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;	
	}
	public function getOrderProcess(){
		//$id = $this->url->segment(3);
		$sendDt = $this->input->get('sendDt');
		$dlvMthdCd = $this->input->get('dlvMthdCd');
		$dlvEtprsCd = $this->input->get('dlvEtprsCd');
		$invcNo = $this->input->get('invcNo');
		$dlvNo = $this->input->get('dlvNo');
		$url = $this->url."/ordservices/reqdelivery/".$sendDt."/".$dlvMthdCd."/".$dlvEtprsCd."/".$invcNo."/".$dlvNo;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;	
	}
	public function getCompleteOrder(){
		$id = $this->url->segment(3);
		$startTime = $this->input->get('startTime');
		$endTime = $this->input->get('endTime');
		$url = $this->url."/ordservices/dlvcompleted/".$startTime."/".$endTime;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;	
	}
	public function getSalesToProcess(){
		$id = $this->url->segment(3);
		$ordNo = $this->input->get('ordNo');
		$ordPrdSeq = $this->input->get('ordPrdSeq');
		$ordCnRsnCd = $this->input->get('ordCnRsnCd');
		$ordCnDtlsRsn = $this->input->get('ordCnDtlsRsn');
		$url = $this->url."/claimservice/reqrejectorder/".$ordNo."/".$ordPrdSeq."/".$ordCnRsnCd."/".$ordCnDtlsRsn;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;	

	}
	public function getViewStatusPerOrderNumber($ordNo){
		// $ordNo = $this->input->get('ordNo');

		// echo $url = $this->url."/ordservices/complete".$ordNo;
		// echo $result = $this->restclient->get($url,$header);
		// header('Content-Type: application/xml');
		// die;
		// echo $result;	
		$url = $this->url."/ordservices/complete/".$ordNo;
		$result = $this->restclient->get($url,$header);
		header('Content-Type: application/xml');
		echo $result;
	}
	public function test(){
		$data = array(
			'selMthaCd' => $this->input->post('selMthaCd'),
			'dispCtgNo' => $this->input->post('dispCtgNo'),
			'prdTypCd'  => $this->input->post('prdTypCd'),
			'prdNm'     => $this->input->post('prdNm'),
			'prdStatCd' => $this->input->post('prdStatCd'),
			'prdWght'	=> $this->input->post('prdWght'),
			'prdlmage01' => $this->input->post('prdlmage01'),
			'prdlmage02' => $this->input->post('prdlmage02'),
			'prdlmage03' => $this->input->post('prdlmage03'),
			'prdlmage04' => $this->input->post('prdlmage04'),
			'htmlDetail' => $this->input->post('htmlDetail'),
			'orngTypCd'  => $this->input->post('orngTypCd'),
			'orngTypDtlsCd' => $this->input->post('orngTypDtlsCd'),
			'sellerPrdCd'   => $this->input->post('sellerPrdCd'),
			'selTermUseYu'  => $this->input->post('sellerPrdCd'),
			'selPrdClfCd'	=> $this->input->post('selPrdClfCd'),
			'apiBgnDy'		=> $this->input->post('apibgnDy'),
			'apiEndDy'		=> $this->input->post('apiEndDy'),
			'wehsPlnDy'		=> $this->input->post('wehsPlnDy'),
			'selPrc'		=> $this->input->post('selPrc'),
			'prdSelQty'		=> $this->input->post('prdSelQty'),
			'cuponcheck'	=> $this->input->post('cuponcheck'),
			'dscAmtPercnt'	=> $this->input->post('dscAmtPercnt'),
			'cupnDscMthdCd'	=> $this->input->post('cupnDscMthdCd'),
			'cupnUseLmDyYn'	=> $this->input->post('cupnUseLmDyYn'),
			'cupnlssStarDy' => $this->input->post('cupnlssStarDy'),
			'cupnlssEndDy'	=> $this->input->post('cupnlssEndDy'),
			'optWrtCnt'		=> $this->input->post('optWrtCnt'),
			'productWrtOption' => $this->input->post('productWrtOption'),
			'optSelCnt'		   => $this->input->post('optSelCnt'),
			'optionAllQty'	=> $this->input->post('optionAllQty'),
			'optionAllAddWght'	=> $this->input->post('optionAllAddWght'),
			'optionAllAddPrc'	=> $this->input->post('optionAllAddPrc'),
			'colTitle'	=> $this->input->post('colTitle'),
			'productSelOtion'	=> $this->input->post('productSelOtion'),
			'optStatus'	=> $this->input->post('optStatus'),
			'optWght'	=> $this->input->post('optWght'),
			'optPrc'	=> $this->input->post('optPrc'),
			'ProductComponent'	=> $this->input->post('ProductComponent'),
			'addPrdGrpNm'		=> $this->input->post('addPrdGrpNm'),
			'compPrdNm'	=> $this->input->post('compPrdNm'),
			'addCompPrc' => $this->input->post('addCompPrc'),
			'compPrdQty'	=> $this->input->post('compPrdQty'),
			'addUseYn'	=> $this->input->post('addUseYn'),
			'addPrdWght'	=> $this->input->post('addPrdWght'),
			'selMinLimitQty' 	=> $this->input->post('selMinLimitQty'),
			'seLimitTypCd'	=> $this->input->post('seLimitTypCd'),
			'selLimitQty'	=> $this->input->post('selLimitQty'),
			'asDetail'	=> $this->input->post('asDetail'),
			'dlvCndtSeq'	=> $this->input->post('dlvCndtSeq'),
			'rtngExchDetail'	=> $this->input->post('rtngExchDetail'),
			'supIDtyfrPrdClfCd'	=> $this->input->post('supIDtyfrPrdClfCd'),
			'createCd'	=> $this->input->post('createCd'),
		);
	    $data_string = json_encode($data);
	    $url = $this->url.'/prodservices/product';
	    $curl = curl_init($url);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/xml',
	    'Content-Length: ' . strlen($data_string))
	    );
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  
	    $result = curl_exec($curl);
	    curl_close($curl);
	    echo $result;
	}
	public function InsertProductT(){
		// $submit = $this->input->post('submit');
		$spf_id_pk = $this->input->post('spf_id_pk');
		$catid 	   = $this->input->post('catid');
		$spc_id_pk = $this->input->post('spc_id_pk');
		$spd_product_code = $this->input->post('spd_product_code');
		$spd_title_th = $this->input->post('spd_title_th');
		$spd_title_en = $this->input->post('spd_title_en');
		$spd_title_jp = $this->input->post('spd_title_jp');
		$spd_description_th = $this->input->post('spd_description_th');
		$spd_description_en = $this->input->post('spd_description_en');
		$spd_description_jp = $this->input->post('spd_description_jp');
		$spd_brand_th = $this->input->post('spd_brand_th');
		$spd_brand_en = $this->input->post('spd_brand_en');
		$spd_brand_jp = $this->input->post('spd_brand_jp');
		$spd_model_th = $this->input->post('spd_model_th');
		$spd_model_en = $this->input->post('spd_model_en');
		$spd_model_jp = $this->input->post('spd_model_jp');
		$spd_price_th = $this->input->post('spd_price_th');
		$spd_price_en = $this->input->post('spd_price_en');
		$spd_link = $this->input->post('spd_link');
		$spd_ip = $this->input->ip_address();
		$spd_update_date_time = date('Y:M:D');
		$spd_date_time = date('Y:M:D');

		if(isset($submit)){
			echo 'insert';
		}else{
			echo 'rrrr';
		}
	}
	public function test2(){
		$spf_id_pk = $this->input->post('spf_id_pk');
		$catid 	   = $this->input->post('catid');
		$spc_id_pk = $this->input->post('spc_id_pk');
		$spd_product_code = $this->input->post('spd_product_code');
		$spd_title_th = $this->input->post('spd_title_th');
		$spd_title_en = $this->input->post('spd_title_en');
		$spd_title_jp = $this->input->post('spd_title_jp');
		$spd_description_th = $this->input->post('spd_description_th');
		$spd_description_en = $this->input->post('spd_description_en');
		$spd_description_jp = $this->input->post('spd_description_jp');
		$spd_brand_th = $this->input->post('spd_brand_th');
		$spd_brand_en = $this->input->post('spd_brand_en');
		$spd_brand_jp = $this->input->post('spd_brand_jp');
		$spd_model_th = $this->input->post('spd_model_th');
		$spd_model_en = $this->input->post('spd_model_en');
		$spd_model_jp = $this->input->post('spd_model_jp');
		$spd_price_th = $this->input->post('spd_price_th');
		$spd_price_en = $this->input->post('spd_price_en');
		$spd_price_jp = $this->input->post('spd_price_jp');
		$spd_link = $this->input->post('spd_link');
		$spd_ip = $this->input->ip_address();
		$spd_stat = $this->input->post('spd_stat');
		$spd_stat_tellafriend = $this->input->post('spd_stat_tellafriend');
		$spd_picture_more = $this->input->post('spd_picture_more');
		$spd_orderby = $this->input->post('spd_orderby');
		$spd_status = $this->input->post('spd_status');
		$spd_update_date_time = date('Y:M:D');
		$spd_date_time = date('Y:M:D');
		$price_show = $this->input->post('price_show');
		$price_word = $this->input->post('price_word');
		$spd_producttitleeditor = $this->input->post('spd_producttitleeditor');
		$inventory = $this->input->post('inventory');
		$cat1id = $this->input->post('cat1id');
		$spd_weight = $this->input->post('spd_weight');
		$spd_width = $this->input->post('spd_width');
		$spd_length = $this->input->post('spd_length');
		$spd_height  = $this->input->post('spd_height');
		$packageid = $this->input->post('packageid');
		$spd_policy_show = $this->input->post('spd_policy_show');
		$multi_option = $this->input->post('multi_option');
		$shipping_by_product = $this->input->post('shipping_by_product');
		$shipping_type = $this->input->post('shipping_type');
		$shipping_fee = $this->input->post('shipping_fee');
		$datatarad = array(
			'spf_id_pk' => $spf_id_pk,
			'catid'	=> $catid,
			'spc_id_pk'	=> $spc_id_pk,
			'spd_product_code'	=> $spd_product_code,
			'spd_title_th'	=> $spd_title_th,
			'spd_title_en'	=> $spd_title_en,
			'spd_title_jp'	=> $spd_title_jp,
			'spd_description_th'	=> $spd_description_th,
			'spd_description_en'	=> $spd_description_en,
			'spd_description_jp'	=> $spd_description_jp,
			'spd_brand_th'	=> $spd_brand_th,
			'spd_brand_en'	=> $spd_brand_en,
			'spd_brand_jp'	=> $spd_brand_jp,
			'spd_model_th'	=> $spd_model_th,
			'spd_model_en'	=> $spd_model_en,
			'spd_model_jp'	=> $spd_model_jp,
			'spd_price_th'	=> $spd_price_th,
			'spd_price_en'	=> $spd_price_en,
			'spd_price_jp'	=> $spd_price_jp,
			'spd_link'	=> $spd_link,
			'spd_ip'	=> $spd_id,
			'spd_stat'	=> $spd_stat,
			'spd_stat_tellafriend'	=> $spd_stat_tellafriend,
			'spd_picture_more' => $spd_picture_more,
			'spd_orderby' => $spd_orderby,
			'spd_update_date_time' => $spd_update_date_time,
			'spd_date_time'	=> $spd_date_time,
			'price_show'	=> $price_show,
			'price_word'	=> $price_word,
			'spd_producttitleeditor'	=> $spd_producttitleeditor,
			'inventory'	=> $inventory,
			'cat1id'	=> $cat1id,
			'spd_weight'	=> $spd_weight,
			'spd_width'	=> $spd_width,
			'spd_length'	=> $spd_length,
			'spd_height'	=> $spd_height,
			'packageid'	=> $packageid,
			'spd_policy_show' => $spd_policy_show,
			'multi_option'	=> $multi_option,
			'shipping_by_product' => $shipping_by_product,
			'shipping_type'	=> $shipping_type,
			'shipping_fee'	=> $shipping_fee,
		);
		// รอเชื่อม data tarad => 11street
		$data = array(
			'selMthaCd' => $this->input->post('selMthaCd'),
			'dispCtgNo' => $this->input->post('dispCtgNo'),
			'prdTypCd'  => $this->input->post('prdTypCd'),
			'prdNm'     => $this->input->post('prdNm'),
			'prdStatCd' => $this->input->post('prdStatCd'),
			'prdWght'	=> $this->input->post('prdWght'),
			'prdlmage01' => $this->input->post('prdlmage01'),
			'prdlmage02' => $this->input->post('prdlmage02'),
			'prdlmage03' => $this->input->post('prdlmage03'),
			'prdlmage04' => $this->input->post('prdlmage04'),
			'htmlDetail' => $this->input->post('htmlDetail'),
			'orngTypCd'  => $this->input->post('orngTypCd'),
			'orngTypDtlsCd' => $this->input->post('orngTypDtlsCd'),
			'sellerPrdCd'   => $this->input->post('sellerPrdCd'),
			'selTermUseYu'  => $this->input->post('sellerPrdCd'),
			'selPrdClfCd'	=> $this->input->post('selPrdClfCd'),
			'apiBgnDy'		=> $this->input->post('apibgnDy'),
			'apiEndDy'		=> $this->input->post('apiEndDy'),
			'wehsPlnDy'		=> $this->input->post('wehsPlnDy'),
			'selPrc'		=> $this->input->post('selPrc'),
			'prdSelQty'		=> $this->input->post('prdSelQty'),
			'cuponcheck'	=> $this->input->post('cuponcheck'),
			'dscAmtPercnt'	=> $this->input->post('dscAmtPercnt'),
			'cupnDscMthdCd'	=> $this->input->post('cupnDscMthdCd'),
			'cupnUseLmDyYn'	=> $this->input->post('cupnUseLmDyYn'),
			'cupnlssStarDy' => $this->input->post('cupnlssStarDy'),
			'cupnlssEndDy'	=> $this->input->post('cupnlssEndDy'),
			'optWrtCnt'		=> $this->input->post('optWrtCnt'),
			'productWrtOption' => $this->input->post('productWrtOption'),
			'optSelCnt'		   => $this->input->post('optSelCnt'),
			'optionAllQty'	=> $this->input->post('optionAllQty'),
			'optionAllAddWght'	=> $this->input->post('optionAllAddWght'),
			'optionAllAddPrc'	=> $this->input->post('optionAllAddPrc'),
			'colTitle'	=> $this->input->post('colTitle'),
			'productSelOtion'	=> $this->input->post('productSelOtion'),
			'optStatus'	=> $this->input->post('optStatus'),
			'optWght'	=> $this->input->post('optWght'),
			'optPrc'	=> $this->input->post('optPrc'),
			'ProductComponent'	=> $this->input->post('ProductComponent'),
			'addPrdGrpNm'		=> $this->input->post('addPrdGrpNm'),
			'compPrdNm'	=> $this->input->post('compPrdNm'),
			'addCompPrc' => $this->input->post('addCompPrc'),
			'compPrdQty'	=> $this->input->post('compPrdQty'),
			'addUseYn'	=> $this->input->post('addUseYn'),
			'addPrdWght'	=> $this->input->post('addPrdWght'),
			'selMinLimitQty' 	=> $this->input->post('selMinLimitQty'),
			'seLimitTypCd'	=> $this->input->post('seLimitTypCd'),
			'selLimitQty'	=> $this->input->post('selLimitQty'),
			'asDetail'	=> $this->input->post('asDetail'),
			'dlvCndtSeq'	=> $this->input->post('dlvCndtSeq'),
			'rtngExchDetail'	=> $this->input->post('rtngExchDetail'),
			'supIDtyfrPrdClfCd'	=> $this->input->post('supIDtyfrPrdClfCd'),
			'createCd'	=> $this->input->post('createCd'),
		);
		$this->product->insert($datatarad);
	}

}



