<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Street extends CI_Controller {
	private $openapikey = '50667854bb253d281ce0fe36ebaeebaa';
	private $url = 'http://api.11street.co.th/rest';
	private $header;
	private $product;
	public function __construct(){
		parent:: __construct();
		$this->load->library('restclient');
		$this->header   = array(
			"Contenttype"=>"application/xml",
			"AcceptCharset"=>"utf8",
			"openapikey"=>$this->openapikey
		);
		$this->load->library('xml_writer');
		#$this->load->model('product_model');
	}
	public function index(){
		$this->restclient->debug();
	}
	public function category(){
		$url = $this->url."/cateservice/category";
		$result = $this->restclient->get($url, $this->header);
		if(!$result)die($this->restclient->debug());
		header('Content-Type: application/xml');
		echo $result;
	}	
	public function subCategoryByParentId(){
		$parent_id = $this->input->get('parent_id');
		$url = $this->url."/cateservice/category/".$parent_id;
		$result = $this->restclient->get($url,$this->header);
		header('Content-Type: application/xml');
		echo $result;		
	}
	public function updateProduct(){
		$shopId = $this->input->post('shopId');
		$shopId = '5';
		$prdNo = '271761';
		$dom = new DOMDocument('1.0','UTF-8');
        $dom->formatOutput = true;
        $root = $dom->createElement('Product');
        $dom->appendChild($root);
        $root->appendChild( $dom->createElement('advrtStmt', '9789792771251') );
		$root->appendChild( $dom->createElement('selMthdCd', '01') );
		$root->appendChild( $dom->createElement('asDetail', '-') );
		$root->appendChild( $dom->createElement('prdTypCd', '01') );
		$root->appendChild( $dom->createElement('prdNm', 'name') );
		$root->appendChild( $dom->createElement('prdStatCd', '01') );
		$root->appendChild( $dom->createElement('dispCtgrNo', '7030') );
        $root->appendChild( $dom->createElement('bndlDlvCnYn', 'N') );
        $root->appendChild($dom->createElement('cupnDscMthdCd','01'));
		$root->appendChild($dom->createElement('cupnIssEndDy','20/08/2016'));
		$root->appendChild($dom->createElement('cupnIssStartDy','13/08/2016'));
		$root->appendChild($dom->createElement('cupnUseLmtDyYn','Y'));
		$root->appendChild($dom->createElement('cuponcheck','Y'));
		$root->appendChild($dom->createElement('dispCtgrStatCd','03'));
		$root->appendChild($dom->createElement('dscAmtPercnt','25'));
		$root->appendChild($dom->createElement('exchDlvCst','0'));
		$root->appendChild($dom->createElement('htmlDetail','Detail'));
		$root->appendChild($dom->createElement('imageKindChk','01'));
		$root->appendChild($dom->createElement('minorSelCnYn','Y'));
		$root->appendChild($dom->createElement('optWrtCnt','1'));
		$root->appendChild($dom->createElement('orgnNmVal','Korea'));
		$root->appendChild($dom->createElement('outsideYnIn','N'));
		$root->appendChild($dom->createElement('outsideYnOut','N'));
		$root->appendChild($dom->createElement('pointValue','100'));
		$root->appendChild($dom->createElement('pointYN','Y'));
		$root->appendChild($dom->createElement('prdAttrCd',''));
		$root->appendChild($dom->createElement('prdImage01','http://image.11street.co.th/ex_t/R/150x150/0/0/0/src/uiImg/noimg.gif'));
		$root->appendChild($dom->createElement('prdImage02','http://image.11street.co.th/ex_t/R/150x150/0/0/0/src/uiImg/noimg.gif'));
		$root->appendChild($dom->createElement('prdNm','Seller Product'));
		$root->appendChild($dom->createElement('prdSelQty','9999'));
		$root->appendChild($dom->createElement('prdStatCd','01'));
		$root->appendChild($dom->createElement('prdUpdYN','Y'));
		$root->appendChild($dom->createElement('prdWght','1000'));
		$root->appendChild($dom->createElement('preSelPrc','0'));
		//productSelOption
		$productSelOption = $root->appendChild($dom->createElement('productSelOption'));
		$productSelOption->appendChild($dom->createElement('colValue0','Y'));
		$productSelOption->appendChild($dom->createElement('colValue1','Y'));
		$productSelOption->appendChild($dom->createElement('colCount','Y'));
		$productSelOption->appendChild($dom->createElement('addUseYn','Y'));
		$productSelOption->appendChild($dom->createElement('optStatus','01'));
		$productSelOption->appendChild($dom->createElement('optWght','10.22'));
		$productSelOption->appendChild($dom->createElement('optPrc','13'));

		$ProductComponent = $root->appendChild($dom->createElement('ProductComponent'));
		$ProductComponent->appendChild($dom->createElement('addCompPrc','5'));
		$ProductComponent->appendChild($dom->createElement('addPrdGrpNm','Add Product 1'));
		$ProductComponent->appendChild($dom->createElement('addPrdWght','0.234'));
		$ProductComponent->appendChild($dom->createElement('addUseYn','Y'));
		$ProductComponent->appendChild($dom->createElement('compPrdNm','1G'));
		$ProductComponent->appendChild($dom->createElement('compPrdQty','5'));
//2
		$ProductComponent = $root->appendChild($dom->createElement('ProductComponent'));
		$ProductComponent->appendChild($dom->createElement('addCompPrc','6'));
		$ProductComponent->appendChild($dom->createElement('addPrdGrpNm','add p2'));
		$ProductComponent->appendChild($dom->createElement('addPrdWght','0.245'));
		$ProductComponent->appendChild($dom->createElement('addUseYn','Y'));
		$ProductComponent->appendChild($dom->createElement('compPrdNm','2G'));
		$ProductComponent->appendChild($dom->createElement('compPrdQty','10'));

		$productWrtOption = $root->appendChild($dom->createElement('productWrtOption'));
		$productWrtOption->appendChild($dom->createElement('colValue0','Y'));
		$productWrtOption->appendChild($dom->createElement('colValue1','Y'));
		$root->appendChild($dom->createElement('colTitle','Y'));
		$root->appendChild($dom->createElement('proxyYn','N'));
		$root->appendChild($dom->createElement('rootCtgrNo','0'));
		$root->appendChild($dom->createElement('rtngExchDetail','test sample'));
		$root->appendChild($dom->createElement('rtngdDlvCst','0'));
		$root->appendChild($dom->createElement('selLimitQty','10'));
		$root->appendChild($dom->createElement('selLimitTypCd','02'));
		$root->appendChild($dom->createElement('selMinLimitQty','1111'));
		$root->appendChild($dom->createElement('selMinLimitTypCd','01'));
		$root->appendChild($dom->createElement('selMnbdNckNm',''));
		$root->appendChild($dom->createElement('selMthdCd','01'));
		$root->appendChild($dom->createElement('selPrc','100'));
		$root->appendChild($dom->createElement('sellerItemEventYn','N'));
		$root->appendChild($dom->createElement('sellerPrdCd','815210'));
		// $root->appendChild($dom->createElement('shopNo','0'));
		// $root->appendChild($dom->createElement('spplWyCd','02'));
		$root->appendChild($dom->createElement('suplDtyfrPrdClfCd','01'));
		// $root->appendChild($dom->createElement('validateMsg',''));
		// $root->appendChild($dom->createElement('nResult','0'));
		$root->appendChild($dom->createElement('dlvCndtSeq','12465'));
		$root->appendChild($dom->createElement('selTermUseYn','Y'));
		$root->appendChild($dom->createElement('selPrdClfCd','3:101'));
		$root->appendChild($dom->createElement('aplBgnDy','30/06/2016'));
		$root->appendChild($dom->createElement('aplEndDy','30/06/2018'));
        echo $dataProduct =  $dom->saveXML();
		 $dom->save(''.$shopId.'-'.'result.xml') or die('XML Create Error');
		 $curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "http://api.11street.co.th/rest/prodservices/product/".$prdNo,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $dataProduct,
		CURLOPT_HTTPHEADER => array(
			"AcceptCharset: utf8",
			"Cache-Control: no-cache",
			"Content-Type: application/xml",
			"openapikey: 50667854bb253d281ce0fe36ebaeebaa"
		),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		echo $response;
		}
		echo 'dssssssssssssss';
	}
	public function addProductStreet(){
		$this->xml_writer->addDataProduct();
	}
	public function updateProductStreet(){
		$this->xml_writer->updateProduct();
	}
	public function xx(){
		$simple_xml = new SimpleXMLElement('<student></student>');

		$simple_xml->addChild('result');
		$simple_xml->result[0]->addAttribute('id', 1);

		$simple_xml->result[0]->addChild('name', '1 Kole');
		$simple_xml->result[0]->addChild('sgpa', '8.1');
		$simple_xml->result[0]->addChild('cgpa', '8.4');

		echo $simple_xml->asXML('simple_xml_create.xml');
	}
}