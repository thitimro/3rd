<? php
require APPPATH.'/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Rest.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Street2 extends CI_Controller {
	private $openapikey = '50667854bb253d281ce0fe36ebaeebaa';
	private $url = 'http://api.11street.co.th/rest';
	private $header;
	private $product;
	
	function __construct() {
		parent::__construct();
		$this - > load - > library('curl');
		$this - > load - > library('restclient');
		$this - > header = array(
			"Contenttype : application/xml",
			"AcceptCharset: utf8",
			"openapikey: 50667854bb253d281ce0fe36ebaeebaa",
		);
		$this - > load - > library('curl');
		$this - > load -
			> add_package_path(FCPATH.
				'vendor/restclient') -
			> library('restclient') -
			> remove_package_path(FCPATH.
				'vendor/restclient');
		$this - > load - > helper('url');
	}
	
	function index() {
		$json = $this - > restclient - > get(site_url('../cateservice/category'), [
			'dispNo' => '7000',
		]);
		$this - > restclient - > debug();
	}
	
	function getCategory() {
		$json = $this - > restclient - > get(site_url('../cateservice/category'));
		$this - > restclient - > debug();
	}
	
	function SubCategory() {
		$parentDispNo = $this->input->post('parentDispNo');
		$json = $this - > restclient - > get(site_url('../cateservice/category/'.$parentDispNo));
		$this - > restclient - > debug();
	}

	
	function addProduct() {#
		// print_r($_POST);
		// die;
		$advrtStmt = $this->input->post('advrtStmt');
		$asDetail = $this->input->post('asDetail');
		$bndlDlvCnYn = $this->input->post('bndlDlvCnYn');
		$cupnDscMthdCd = $this->input->post('cupnDscMthdCd');
		$cupnIssEndDy = $this->input->post('cupnIssEndDy');
		$cupnIssStartDy = $this->input->post('cupnIssStartDy');
		$cupnUseLmtDyYn = $this->input->post('cupnUseLmtDyYn');
		$cuponcheck = $this->input->post('cuponcheck');
		$dispCtgrNo = $this->input->post('dispCtgrNo');
		$dispCtgrStatCd = $this->input->post('dispCtgrStatCd');
		$dscAmtPercnt = $this->input->post('dscAmtPercnt');
		$exchDlvCst = $this->input->post('exchDlvCst');
		$htmlDetail = $this->input->post('htmlDetail');
		$imageKindChk = $this->input->post('imageKindChk');
		$minorSelCnYn = $this->input->post('minorSelCnYn');
		$optWrtCnt = $this->input->post('optWrtCnt');
		$orgnNmVal = $this->input->post('orgnNmVal');
		$outsideYnIn = $this->input->post('outsideYnIn');
		$outsideYnOut = $this->input->post('outsideYnOut');
		$pointValue = $this->input->post('pointValue');
		$pointYN = $this->input->post('pointYN');
		$prdImage01 = $this->input->post('prdImage01');
		$prdImage02 = $this->input->post('prdImage02');
		$prdNm = $this ->input->post('prdNm');
		$prdSelQty = $this->input->post('prdSelQty');
		$prdStatCd = $this->input->post('prdStatCd');
		$prdTypCd = $this->input->post('prdTypCd');
		$prdUpdYN = $this->input->post('prdUpdYN');
		$prdWght = $this->input->post('prdWght');
		$preSelPrc = $this->input->post('preSelPrc');
		$addPrdGrpNm = $this->input->post('addPrdGrpNm');
		$addPrdWght = $this->input->post('addPrdWght');
		$addUseYn = $this->input->post('addUseYn');
		$compPrdNm = $this->input->post('compPrdNm');
		$compPrdQty = $this->input->post('compPrdQty');
		$preSelPrc = $this->input->post('preSelPrc');
		$addCompPrc = $this->input->post('addCompPrc');
		$addPrdGrpNm2 = $this->input->post('addPrdGrpNm2');
		$addPrdWght2 = $this->input->post('addPrdWght2');
		$addUseYn2 = $this->input->post('addUseYn2');
		$compPrdNm2 = $this->input->post('compPrdNm2');
		$compPrdQty2 = $this->input->post('compPrdQty2');
		$addCompPrc2 = $this->input->post('addCompPrc2');
		$colValue0 = $this->input->post('colValue0');
		$proxyYn = $this->input->post('proxyYn');
		$rootCtgrNo = $this-> input->post('rootCtgrNo');
		$rtngExchDetail = $this-> input->post('rtngExchDetail');
		$rtngdDlvCst = $this->input->post('rtngdDlvCst');
		$selLimitQty = $this->input->post('selLimitQty');
		$selLimitTypCd = $this->input->post('selLimitTypCd');
		$selMinLimitQty = $this->input->post('selMinLimitQty');
		$selMinLimitTypCd = $this->input->post('selMinLimitTypCd');
		$selMthdCd = $this->input->post('selMthdCd');
		$selPrc = $this->input->post('selPrc');
		$sellerItemEventYn = $this->input->post('sellerItemEventYn');
		$sellerPrdCd = $this->input->post('sellerPrdCd');
		$shopNo = $this->input->post('shopNo');
		$spplWyCd = $this ->input->post('spplWyCd');
		$suplDtyfrPrdClfCd = $this->input->post('suplDtyfrPrdClfCd');
		$nResult = $this->input->post('nResult');
		$dlvCndtSeq = $this->input->post('dlvCndtSeq');
		$selTermUseYn = $this->input->post('selTermUseYn');
		$selPrdClfCd = $this->input->post('selPrdClfCd');
		$aplBgnDy = $this->input->post('aplBgnDy');
		$aplEndDy = $this->input->post('aplEndDy');
		$xml_data = '<Product>'.
		'<advrtStmt>'.$advrtStmt.'</advrtStmt>'.
		'<asDetail>'.$asDetail.'</asDetail>'.
		'<bndlDlvCnYn>'.$bndlDlvCnYn.'</bndlDlvCnYn>'.
		'<cupnDscMthdCd>'.$cupnDscMthdCd.'</cupnDscMthdCd>'.
		'<cupnIssEndDy>'.$cupnIssEndDy.'</cupnIssEndDy>'.
		'<cupnIssStartDy>'.$cupnIssStartDy.'</cupnIssStartDy>'.
		'<cupnUseLmtDyYn>'.$cupnUseLmtDyYn.'</cupnUseLmtDyYn>'.'<cuponcheck>'.$cuponcheck.
		'</cuponcheck>'.
		'<dispCtgrNo>'.$dispCtgrNo.'</dispCtgrNo>'.
		'<dispCtgrStatCd>'.$dispCtgrStatCd.'</dispCtgrStatCd>'.
		'<dscAmtPercnt>'.$dscAmtPercnt.'</dscAmtPercnt>'.
		'<exchDlvCst>'.$exchDlvCst.'</exchDlvCst>'.
		'<htmlDetail>'.$htmlDetail.'</htmlDetail>'.
		'<imageKindChk>'.$imageKindChk.'</imageKindChk>'.
		'<minorSelCnYn>'.$minorSelCnYn.'</minorSelCnYn>'.
		'<optWrtCnt>'.$optWrtCnt.'</optWrtCnt>'.
		'<orgnNmVal>'.$orgnNmVal.'</orgnNmVal>'.
		'<outsideYnIn>'.$outsideYnIn.'</outsideYnIn>'.
		'<outsideYnOut>'.$outsideYnOut.'</outsideYnOut>'.
		'<pointValue>'.$pointValue.'</pointValue>'.
		'<pointYN>'.$pointYN.'</pointYN>'.
		'<prdAttrCd />'.
		'<prdImage01>'.$prdImage01.'</prdImage01 >'.
		'<prdImage02>'.$prdImage02.'</prdImage02 >'.
		'<prdNm>'.$prdNm.'</prdNm>'.
		'<prdSelQty>'.$prdSelQty.'</prdSelQty>'.
		'<prdStatCd>'.$prdStatCd.'</prdStatCd>'.
		'<prdTypCd>'.$prdTypCd.'</prdTypCd>'.
		'<prdUpdYN>'.$prdUpdYN.'</prdUpdYN>'.
		'<prdWght>'.$prdWght.'</prdWght>'.
		'<preSelPrc>'.$preSelPrc.'</preSelPrc>'.
		'<ProductComponent>'.
		'<addCompPrc>'.$addCompPrc.'</addCompPrc>'.
		'<addPrdGrpNm>'.$addPrdGrpNm.'</addPrdGrpNm>'.
		'<addPrdWght>'.$addPrdWght.'</addPrdWght>'.
		'<addUseYn>'.$addUseYn.'</addUseYn>'.
		'<compPrdNm>'.$compPrdNm.'</compPrdNm>'.
		'<compPrdQty>'.$compPrdQty.'</compPrdQty>'.
		'</ProductComponent>'.
		'<ProductComponent>'.
		'<addCompPrc>'.$addCompPrc2.'</addCompPrc>'.
		'<addPrdGrpNm>'.$addPrdGrpNm2.'</addPrdGrpNm>'.
		'<addPrdWght>'.$addPrdGrpNm2.'</addPrdWght>'.
		'<addUseYn>'.$addUseYn2.'</addUseYn>'.
		'<compPrdNm>'.$compPrdNm2.'</compPrdNm>'.
		'<compPrdQty>'.$compPrdQty2.'</compPrdQty>'.
		'</ProductComponent>'.
		'<productWrtOption>'.
		'<colValue0>'.$colValue0.'</colValue0>'.
		'</productWrtOption>'.
		'<proxyYn>'.$proxyYn.'</proxyYn>'.
		'<rootCtgrNo>'.$rootCtgrNo.'</rootCtgrNo>'.
		'<rtngExchDetail>'.$rtngExchDetail.'</rtngExchDetail>'.
		'<rtngdDlvCst>'.$rtngdDlvCst.'</rtngdDlvCst>'.
		'<selLimitQty>'.$selLimitQty.'</selLimitQty>'.
		'<selLimitTypCd>'.$selLimitTypCd.'</selLimitTypCd>'.
		'<selMinLimitQty>'.$selMinLimitQty.'</selMinLimitQty>'.
		'<selMinLimitTypCd>'.$selMinLimitTypCd.'</selMinLimitTypCd>'.
		'<selMnbdNckNm />'.
		'<selMthdCd>'.$selMthdCd.'</selMthdCd>'.
		'<selPrc>'.$selPrc.'</selPrc>'.
		'<sellerItemEventYn>'.$sellerItemEventYn.'</sellerItemEventYn>'.
		'<sellerPrdCd>'.$sellerPrdCd.'</sellerPrdCd>'.
		'<shopNo>'.$shopNo.'</shopNo>'.
		'<spplWyCd>'.$spplWyCd.'</spplWyCd>'.
		'<suplDtyfrPrdClfCd>'.$suplDtyfrPrdClfCd.'</suplDtyfrPrdClfCd>'.
		'<validateMsg />'.
		'<nResult>'.$nResult.'</nResult>'.
		'<dlvCndtSeq>'.$dlvCndtSeq.'</dlvCndtSeq>'.
		'<selTermUseYn>'.$selTermUseYn.'</selTermUseYn>'.
		'<selPrdClfCd>'.$selPrdClfCd.'</selPrdClfCd>'.
		'<aplBgnDy>'.$aplBgnDy.'</aplBgnDy>'.
		'<aplEndDy>'.$aplEndDy.'</aplEndDy>'.
		'</Product>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://api.11street.co.th/rest/prodservices/product");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Acceptcharset: utf8";
		$headers[] = "Cache-Control: no-cache";
		$headers[] = "Contenttype: application/xml";
		$headers[] = "Openapikey: 50667854bb253d281ce0fe36ebaeebaa";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$output = curl_exec($ch);
		var_dump($output);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:'.curl_error($ch);
		}
		curl_close($ch);
	}
	
	function updateProduct() {
		$advrtStmt = $this->input->post('advrtStmt');
		$asDetail = $this->input->post('asDetail');
		$bndlDlvCnYn = $this->input->post('bndlDlvCnYn');
		$cupnDscMthdCd = $this->input->post('cupnDscMthdCd');
		$cupnIssEndDy = $this->input->post('cupnIssEndDy');
		$cupnIssStartDy = $this->input->post('cupnIssStartDy');
		$cupnUseLmtDyYn = $this->input->post('cupnUseLmtDyYn');
		$cuponcheck = $this->input->post('cuponcheck');
		$dispCtgrNo = $this->input->post('dispCtgrNo');
		$dispCtgrStatCd = $this->input->post('dispCtgrStatCd');
		$dscAmtPercnt = $this->input->post('dscAmtPercnt');
		$exchDlvCst = $this->input->post('exchDlvCst');
		$htmlDetail = $this->input->post('htmlDetail');
		$imageKindChk = $this->input->post('imageKindChk');
		$minorSelCnYn = $this->input->post('minorSelCnYn');
		$optWrtCnt = $this->input->post('optWrtCnt');
		$orgnNmVal = $this->input->post('orgnNmVal');
		$outsideYnIn = $this->input->post('outsideYnIn');
		$outsideYnOut = $this->input->post('outsideYnOut');
		$pointValue = $this->input->post('pointValue');
		$pointYN = $this->input->post('pointYN');
		$prdImage01 = $this->input->post('prdImage01');
		$prdImage02 = $this->input->post('prdImage02');
		$prdNm = $this->input->post('prdNm');
		$prdSelQty = $this->input->post('prdSelQty');
		$prdStatCd = $this->input->post('prdStatCd');
		$prdTypCd = $this->input->post('prdTypCd');
		$prdUpdYN = $this->input->post('prdUpdYN');
		$prdWght = $this->input->post('prdWght');
		$preSelPrc = $this->input->post('preSelPrc');
		$addPrdGrpNm = $this->input->post('addPrdGrpNm');
		$addPrdWght = $this->input->post('addPrdWght');
		$addUseYn = $this->input->post('addUseYn');
		$compPrdNm = $this->input->post('compPrdNm');
		$compPrdQty = $this->input->post('compPrdQty');
		$preSelPrc = $this->input->post('preSelPrc');
		$addCompPrc = $this->input->post('addCompPrc');
		$addPrdGrpNm2 = $this->input->post('addPrdGrpNm2');
		$addPrdWght2 = $this->input->post('addPrdWght2');
		$addUseYn2 = $this->input->post('addUseYn2');
		$compPrdNm2 = $this->input->post('compPrdNm2');
		$compPrdQty2 = $this->input->post('compPrdQty2');
		$addCompPrc2 = $this->input->post('addCompPrc2');
		$colValue0 = $this->input->post('colValue0');
		$proxyYn = $this->input->post('proxyYn');
		$rootCtgrNo = $this->input->post('rootCtgrNo');
		$rtngExchDetail = $this->input->post('rtngExchDetail');
		$rtngdDlvCst = $this->input->post('rtngdDlvCst');
		$selLimitQty = $this->input->post('selLimitQty');
		$selLimitTypCd = $this->input->post('selLimitTypCd');
		$selMinLimitQty = $this->input->post('selMinLimitQty');
		$selMinLimitTypCd = $this->input->post('selMinLimitTypCd');
		$selMthdCd = $this->input->post('selMthdCd');
		$selPrc = $this->input->post('selPrc');
		$sellerItemEventYn = $this->input->post('sellerItemEventYn');
		$sellerPrdCd = $this->input->post('sellerPrdCd');
		$shopNo = $this->input->post('shopNo');
		$spplWyCd = $this->input->post('spplWyCd');
		$suplDtyfrPrdClfCd = $this->input->post('suplDtyfrPrdClfCd');
		$nResult = $this->input->post('nResult');
		$dlvCndtSeq = $this->input->post('dlvCndtSeq');
		$selTermUseYn = $this->input->post('selTermUseYn');
		$selPrdClfCd = $this->input->post('selPrdClfCd');
		$aplBgnDy = $this->input->post('aplBgnDy');
		$aplEndDy = $this->input->post('aplEndDy');
		$product = '<Product>'.
		'<advrtStmt>'.$advrtStmt.
		'</advrtStmt>'.
		'<asDetail>'.$asDetail.
		'</asDetail>'.
		'<bndlDlvCnYn>'.$bndlDlvCnYn.
		'</bndlDlvCnYn>'.
		'<cupnDscMthdCd>'.$cupnDscMthdCd.
		'</cupnDscMthdCd>'.
		'<cupnIssEndDy>'.$cupnIssEndDy.
		'</cupnIssEndDy>'.
		'<cupnIssStartDy>'.$cupnIssStartDy.
		'</cupnIssStartDy>'.
		'<cupnUseLmtDyYn>'.$cupnUseLmtDyYn.
		'</cupnUseLmtDyYn>'.
		'<cuponcheck>'.$cuponcheck.
		'</cuponcheck>'.
		'<dispCtgrNo>'.$dispCtgrNo.
		'</dispCtgrNo>'.
		'<dispCtgrStatCd>'.$dispCtgrStatCd.
		'</dispCtgrStatCd>'.
		'<dscAmtPercnt>'.$dscAmtPercnt.
		'</dscAmtPercnt>'.
		'<exchDlvCst>'.$exchDlvCst.
		'</exchDlvCst>'.
		'<htmlDetail>'.$htmlDetail.
		'</htmlDetail>'.
		'<imageKindChk>'.$imageKindChk.
		'</imageKindChk>'.
		'<minorSelCnYn>'.$minorSelCnYn.
		'</minorSelCnYn>'.
		'<optWrtCnt>'.$optWrtCnt.
		'</optWrtCnt>'.
		'<orgnNmVal>'.$orgnNmVal.
		'</orgnNmVal>'.
		'<outsideYnIn>'.$outsideYnIn.
		'</outsideYnIn>'.
		'<outsideYnOut>'.$outsideYnOut.
		'</outsideYnOut>'.
		'<pointValue>'.$pointValue.
		'</pointValue>'.
		'<pointYN>'.$pointYN.
		'</pointYN>'.
		'<prdAttrCd />'.

		'<prdImage01>'.$prdImage01.
		'</prdImage01 >
		'.

		'<prdImage02>'.$prdImage02.
		'</prdImage02 >
		'.
		'<prdNm>'.$prdNm.
		'</prdNm>'.
		'<prdSelQty>'.$prdSelQty.
		'</prdSelQty>'.
		'<prdStatCd>'.$prdStatCd.
		'</prdStatCd>'.
		'<prdTypCd>'.$prdTypCd.
		'</prdTypCd>'.
		'<prdUpdYN>'.$prdUpdYN.
		'</prdUpdYN>'.
		'<prdWght>'.$prdWght.
		'</prdWght>'.
		'<preSelPrc>'.$preSelPrc.
		'</preSelPrc>'.
		'<ProductComponent>'.
		'<addCompPrc>'.$addCompPrc.
		'</addCompPrc>'.
		'<addPrdGrpNm>'.$addPrdGrpNm.
		'</addPrdGrpNm>'.
		'<addPrdWght>'.$addPrdWght.
		'</addPrdWght>'.
		'<addUseYn>'.$addUseYn.
		'</addUseYn>'.
		'<compPrdNm>'.$compPrdNm.
		'</compPrdNm>'.
		'<compPrdQty>'.$compPrdQty.
		'</compPrdQty>'.
		'</ProductComponent>'.
		'<ProductComponent>'.
		'<addCompPrc>'.$addCompPrc2.
		'</addCompPrc>'.
		'<addPrdGrpNm>'.$addPrdGrpNm2.
		'</addPrdGrpNm>'.
		'<addPrdWght>'.$addPrdGrpNm2.
		'</addPrdWght>'.
		'<addUseYn>'.$addUseYn2.
		'</addUseYn>'.
		'<compPrdNm>'.$compPrdNm2.
		'</compPrdNm>'.
		'<compPrdQty>5</compPrdQty>'.
		'</ProductComponent>'.
		'<productWrtOption>'.
		'<colValue0>'.$colValue0.
		'</colValue0>'.
		'</productWrtOption>'.
		'<proxyYn>'.$proxyYn.
		'</proxyYn>'.
		'<rootCtgrNo>'.$rootCtgrNo.
		'</rootCtgrNo>'.
		'<rtngExchDetail>'.$rtngExchDetail.
		'</rtngExchDetail>'.
		'<rtngdDlvCst>'.$rtngdDlvCst.
		'</rtngdDlvCst>'.
		'<selLimitQty>'.$selLimitQty.
		'</selLimitQty>'.
		'<selLimitTypCd>'.$selLimitTypCd.
		'</selLimitTypCd>'.
		'<selMinLimitQty>'.$selMinLimitQty.
		'</selMinLimitQty>'.
		'<selMinLimitTypCd>'.$selMinLimitTypCd.
		'</selMinLimitTypCd>'.
		'<selMnbdNckNm />'.
		'<selMthdCd>'.$selMthdCd.
		'</selMthdCd>'.
		'<selPrc>'.$selPrc.
		'</selPrc>'.
		'<sellerItemEventYn>'.$sellerItemEventYn.
		'</sellerItemEventYn>'.
		'<sellerPrdCd>'.$sellerPrdCd.
		'</sellerPrdCd>'.
		'<shopNo>'.$shopNo.
		'</shopNo>'.
		'<spplWyCd>'.$spplWyCd.
		'</spplWyCd>'.
		'<suplDtyfrPrdClfCd>'.$suplDtyfrPrdClfCd.
		'</suplDtyfrPrdClfCd>'.
		'<validateMsg />'.
		'<nResult>'.$nResult.
		'</nResult>'.
		'<dlvCndtSeq>'.$dlvCndtSeq.
		'</dlvCndtSeq>'.
		'<selTermUseYn>'.$selTermUseYn.
		'</selTermUseYn>'.
		'<selPrdClfCd>'.$selPrdClfCd.
		'</selPrdClfCd>'.
		'<aplBgnDy>'.$aplBgnDy.
		'</aplBgnDy>'.
		'<aplEndDy>'.$aplEndDy.
		'</aplEndDy>'.
		'</Product>';
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.11street.co.th/rest/prodservices/product/7195",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $product,
			CURLOPT_HTTPHEADER => array(
				"AcceptCharset: utf8",
				"Cache-Control: no-cache",
				"Content-Type: application/xml",
				"openapikey: 50667854bb253d281ce0fe36ebaeebaa"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:".$err;
		} else {
			echo $response;
		}
	}
}