<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller{
    private $openapikey = '50667854bb253d281ce0fe36ebaeebaa';
	private $url = 'http://api.11street.co.th/rest';
	private $header;
	private $product;
	public function __construct(){
		parent:: __construct();
		$this->load->library('restclient');
		$this->header   = array(
			"Contenttype"=>"application/xml",
			"AcceptCharset"=>"utf8",
			"openapikey"=>$this->openapikey
		);
		$this->load->model('product_model');
	}
    public function index(){
        $proNo = $this->input->post('proNo');
        $this->load
            ->add_package_path(FCPATH.'vendor/restclient')
            ->library('restclient')
            ->remove_package_path(FCPATH.'vendor/restclient');

        $this->load->helper('url');

        $json = $this->restclient->get(site_url(''), [
                "Contenttype"=>"application/xml",
                "AcceptCharset"=>"utf8",
                // 'productStock'  => $this->input->post('productStock'),
                // 'productStocks' => $this->input->post('productStocks'),
        ]);

        $this->restclient->debug();

        // $url = $this->url."/prodservices/updateProductOption/".$prdNo;
		// $result = $this->restclient->post($url,$this->header,$json);
		// header('Content-Type: application/xml');
		// echo $result;
    }
    public function test(){
        echo 'test';
    }
}