<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Xml extends CI_Controller
{

    function __construct ()
    {
        parent::__construct();
        $this->load->library('xml_writer');
    }

    function index (){
        
        // Load XML writer library
        $this->load->library('xml_writer');
        $data = array(
            'advrtStmt' => $this->input->post('advrtStmt'),
            'asDetail'  => $this->input->post('asDetail'),
            'bndlDlvCnYn'   => $this->input->post('bndlDlvCnYn'),
            'cupnDscMthdCd' => $this->input->post('cupnDscMthdCd'),
        );
        $data['product'] = 'asd';
        $this->xml_weiter->dataProduct($data);
        // Initiate class
        $xml = new Xml_writer();
        $xml->setRootName('Product');
        $xml->initiate();
        $xml->addNode('advrtStmt','9789792771251');
        $xml->addNode('asDetail','-');
        $xml->addNode('bndlDlvCnYn','N');
        $xml->addNode('cupnDscMthdCd','01');
        $xml->addNode('cupnIssEndDy','20/08/2015');
        $xml->addNode('cupnIssStartDy','13/08/2019');
        $xml->addNode('cupnUseLmtDyYn','Y');
        $xml->addNode('cuponcheck','Y');
        $xml->addNode('dispCtgrNo','4955');
        $xml->addNode('dispCtgrStatCd','03');
        $xml->addNode('dscAmtPercnt','25');
        $xml->addNode('exchDlvCst','0');
        $xml->addNode('htmlDetail','html');
        $xml->addNode('imageKindChk','01');
        $xml->addNode('minorSelCnYn','Y');
        $xml->addNode('optWrtCnt','1');
        $xml->addNode('orgnNmVal','Korea');
        $xml->addNode('outsideYnIn','N');
        $xml->addnode('outsideYnOut','N');
        $xml->addNode('pointValue','100');
        $xml->addNode('pointYN','Y');
        $xml->addNode('prdAttrCd','');
        $xml->addNode('prdImage01','http://image.11street.co.th/ex_t/R/150x150/0/0/0/src/uiImg/noimg.gif');
        $xml->addNode('prdImage02','http://image.11street.co.th/ex_t/R/150x150/0/0/0/src/uiImg/noimg.gif');
        $xml->addNode('prdNm','Seller Product');
        $xml->addNode('prdSelQty','9999');
        $xml->addNode('prdStatCd','01');
        $xml->addNode('prdTypCd','01');
        $xml->addNode('prdUpdYN','Y');
        $xml->addNode('prdWght','1000');
        $xml->addNode('preSelPrc','0');
        
        // Set children for branch 'ProductComponent'
        $xml->startBranch('ProductComponent');
        $xml->addNode('addCompPrc', '5');
        $xml->addNode('addPrdGrpNm','addProduct1');
        $xml->addNode('addUseYn','Y');
        $xml->addNode('compPrdNm','1G');
        $xml->addNode('compPrdQty','15');
        $xml->endBranch();
        // 2data
        $xml->startBranch('ProductComponent');
        $xml->addNode('addCompPrc', '5');
        $xml->addNode('addPrdGrpNm','addProduct1');
        $xml->addNode('addUseYn','Y');
        $xml->addNode('compPrdNm','12G');
        $xml->addNode('compPrdQty','13');
        $xml->endBranch();

        //productWrtOption
        $xml->startBranch('productWrtOption');
        $xml->addNode('colValue0','colValue0');
        $xml->endBranch();

        $xml->addNode('proxyYn','N');
        $xml->addNode('rootCtgrNo','0');
        $xml->addNode('rtngExchDetail','test');
        $xml->addNode('rtngdDlvCst','0');
        $xml->addNode('selLimitQty','10');
        $xml->addNode('selLimitTypCd','02');
        $xml->addNode('selMinLimitQty','1');
        $xml->addNode('selMinLimitTypCd','01');
        $xml->addNode('selMnbdNckNm','');
        $xml->addNode('selMthdCd','01');
        $xml->addNode('selPrc','01');
        $xml->addNode('sellerItemEventYn','N');
        $xml->addNode('sellerPrdCd','815210');
        $xml->addNode('shopNo','0');
        $xml->addNode('spplWyCd','02');
        $xml->addNode('suplDtyfrPrdClfCd','01');
        $xml->addNode('validateMsg','');
        $xml->addNode('nResult','0');
        $xml->addNode('dlvCndtSeq','12465');
        $xml->addNode('selTermUseYn','Y');
        $xml->addNode('selPrdClfCd','3:101');
        $xml->addNode('aplBgnDy','30/06/2016');
        $xml->addNode('aplEndDy','');
        




        // $xml->startBranch('bikes');
        
        // // Set children for branch 'cars'
        // // Code is indented to clarify relations.
        // $xml->startBranch('bike', array('country' => 'usa'));
        // $xml->addNode('make', 'Harley-Davidson');
        // $xml->addNode('model', 'Soft tail', array(), true);
        //     $xml->startBranch('parts');
        //         $xml->startBranch('part', array('type' => 'exhaust'));
        //         $xml->addNode('id', '2323-012');
        //         $xml->endBranch();
        //         $xml->startBranch('part', array('type' => 'exhaust'));
        //         $xml->addNode('id', '2323-013');
        //         $xml->endBranch();
        //         $xml->startBranch('part', array('type' => 'carburator'));
        //         $xml->addNode('id', '2541-016');
        //         $xml->endBranch();
        //     $xml->endBranch();
        $xml->endBranch();
        
        // $xml->startBranch('bike', array('country' => 'japan'));
        // $xml->addNode('make', 'BMS');
        // $xml->addNode('model', 'R75', array(), true);
        // $xml->endBranch();
        
        // // End branch 'bikes'
        // $xml->endBranch();
        
        // Pass the XML to the view
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        #$this->load->view('xml', $data);
     #   print_r($data['xml']);
    }
    public function teet(){

     $this->xml_writer->dataProduct();
   
    }
}
