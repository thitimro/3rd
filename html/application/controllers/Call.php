<?php

require APPPATH . '/libraries/REST_Controller.php';

class Call extends REST_Controller
{
   public function news_get()
   {
     //Web service of type GET method
     $this->response(["Hello World"], REST_Controller::HTTP_OK);
   }
   public function news_post()
   {  
     //Web service of type POST method
     $this->response(["Hello World"], REST_Controller::HTTP_OK);
   }
   public function news_put()
   {  
     //Web service of type PUT method
     $this->response(["Hello World"], REST_Controller::HTTP_OK);
   }
   public function news_delete()
   {  
     //Web service of type DELETE method
     $this->response(["Hello World"], REST_Controller::HTTP_OK);
   }
}