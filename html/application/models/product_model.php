<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function read(){
        $query = $this->full->query("select * from `shop_product`");
        return $query->result_array();
   }
   public function readById($id){
        $this->full->where('spd_id_pk=',$id);               
        return $this->full->get('shop_product')->result();   
   }
   public function insert($data){
        $this->db->insert('shop_product',$data);
   }
   public function update($id,$data){ 
        $this->db->where('spd_id_pk',$id);
        $this->db->update('shop_product',$data);
   }
   public function delete($id){
        $result =  $this->db->query("DELETE FROM shop_product WHERE spd_id_pk = $id");
        if($result){
            return "Data is deleted successfully";
        }else{
            return "Error has occurred";
        }
   }
}
